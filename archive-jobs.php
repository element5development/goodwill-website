<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES & CATEGORY FOR BLOG
*/ ?>

<?php get_header(); ?>

<main class="page-contents-container full-width grey-bg">

  <!-- PAGE TITLE -->
  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <!--SKIP NAV -->
  <a id="main-anchor" class="page-anchor"></a>

  <!-- MAIN CONTACT -->
  <div class="hr-contact">
    <div class="flex-container max-width">
      <div class="one-half">
        <img src="/wp-content/uploads/2017/04/hr-contact.jpg" alt="hr contact"/>
      </div>
      <div class="one-half">
          <h2>Contact the<br/>Human Resources (HR) Department</h2>
          <a href="tel:+1-313-557-8766" class="btn primry-btn phone"><span>Call 313-557-8766</span></a><a href="mailto:jobs@goodwilldetroit.org" class="btn primry-btn email"><span>Email</span></a>
      </div>
    </div>
  </div>
  <h2 style="text-align: center;margin-top: 8rem;">Current Goodwill Job Openings</h2>


  <!-- SECONDARY NAVIGATION -->
  <?php get_template_part( 'template-parts/content', 'secondary-nav' ); ?>

  <?php 
    $orderby = $_GET["orderby"];
    $order = $_GET["order"];
    $location = $_GET["location"];
    $term = $_GET["term"];
  ?>
  <section class="blog-feed post-feed max-width">
    <?php if ( $order && $orderby ) { ?>
      <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="jobs" order="'.$order.'" orderby="'.$orderby.'" posts_per_page="6" scroll="false" button_label="Load More"]') ?>
    <?php } elseif ( $location ) { ?>
      <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="jobs" meta_key="city_state" meta_value="'.$location.'" meta_compare="LIKE" posts_per_page="6" scroll="false" button_label="Load More"]') ?>
    <?php } elseif ( $term ) { ?>
      <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="jobs" meta_key="term" meta_value="'.$term.'" meta_compare="LIKE" posts_per_page="6" scroll="false" button_label="Load More"]') ?>
    <?php } else { ?>
      <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="jobs" posts_per_page="6" scroll="false" button_label="Load More"]') ?>
    <?php } ?>
  </section>

  <!-- BACK TO TOP -->
  <a href="#main-anchor" class="back-to-top skip smoothScroll hidden">
    <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
      <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
    </svg>
    <span>Back to Top</span>
  </a>

</main>

<?php get_footer(); ?>