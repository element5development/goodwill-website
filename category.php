<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES & CATEGORY FOR BLOG
*/ ?>

<?php 
$thiscat =  get_query_var('cat'); // The id of the current category
$catobject = get_category($thiscat,false); // Get the Category object by the id of current category
$parentcat = $catobject->category_parent; // the id of the parent category 
?>

<?php get_header(); ?>

<main class="page-contents-container full-width grey-bg">

    <!-- PAGE TITLE -->
    <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

    <!--SKIP NAV -->
    <a id="main-anchor" class="page-anchor"></a>

    <?php if ( $parentcat == 16 || $parentcat == 12 ) { //JOB CATEGORIES ?>
      <!-- MAIN CONTACT -->
      <div class="hr-contact">
        <div class="flex-container max-width">
          <div class="one-half">
            <img src="/wp-content/uploads/2017/03/hr-contact.png" />
          </div>
          <div class="one-half">
              <h2>Contact the<br/>Human Resources (HR) Department</h2>
              <a href="tel:+1<?php the_field('phone','options'); ?>" class="btn primry-btn phone"><span>Call <?php the_field('phone','options'); ?></span></a><a href="mailto:<?php the_field('email','options'); ?>" class="btn primry-btn email"><span>Email</span></a>
          </div>
        </div>
      </div>
      <h2 style="text-align: center;margin-top: 8rem;">Current Goodwill Job Openings</h2>
    <?php } ?>


    <!-- SECONDARY NAVIGATION -->
    <?php get_template_part( 'template-parts/content', 'secondary-nav' ); ?>

    <section class="blog-feed post-feed max-width">
      <?php  
        $cat = get_query_var('cat');
        $category = get_category ($cat);
      ?>
      <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post,jobs" posts_per_page="6" category="'.$category->slug.'" scroll="false" button_label="Load More"]') ?>
    </section>

  <!-- BACK TO TOP -->
  <a href="#main-anchor" class="back-to-top skip smoothScroll hidden">
    <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
      <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
    </svg>
  </a>

</main>

<?php get_footer(); ?>