<?php /*
THE FOOTER TEMPLATE FOR OUR THEME
*/ ?>
      
<!-- SITE NAVIGATION -->
  <?php get_template_part( 'template-parts/content', 'page-footer' ); ?>

<!-- FONT-SIZE COOKIE -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/js.cookie.js"></script>
<!-- SMOOTH SCROLLING -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/smoothscroll.min.js"></script>
<!-- STICKY -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.sticky-kit.min.js"></script>
<!-- ENTRANCE ANIMATIONS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.viewportchecker.min.js"></script>
<!-- SLICK SLIDER -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/slick.min.js"></script>
<!-- FLUIDBOX -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.ba-throttle-debounce.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.fluidbox.min.js"></script>
<!-- LITY -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/lity.min.js"></script>
<!-- MASONRY -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/masonry.pkgd.min.js"></script>
<!-- CHARTS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/Chart.min.js"></script>
<!-- DECLARATIONS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/declarations.min.js"></script>

<script>
  $(function() {
     $.fn.almEmpty = function(alm){
        var el = $(alm.content),
           msg = '<h2 style="text-align: center">Sorry, there are no openings that match your criteria</h2>';
        el.append('<li>'+ msg +'</li>'); // Append to ALM   
        //console.log("Nothing found in this Ajax Load More query :(");
     };
  })(jQuery);
</script>

  
</body>

<?php wp_footer(); ?>

</html>