<?php

//SETUP WORDPRESS & BACKEND
  // WHITELABEL ADMIN AREA
    function my_custom_admin() {
      echo '<style>
        #wpcontent { max-width:100%; min-height: calc(100vh - 110px); }
        .wp-core-ui .button:hover {
          background-color: #e5e5e5 !important;
        }
        .wp-core-ui .button-primary, .wrap .add-new-h2, .wrap .add-new-h2:active, 
        .wrap .page-title-action, .wrap .page-title-action:active {     
          background: #e48849 !Important;
          border-color: #e48849 !important;
          text-shadow: none !important;
          box-shadow: none !important;
          color: #fff !important;
        }
        .wp-core-ui .button-primary:hover, .wrap .add-new-h2:hover,.wrap .page-title-action:hover {
          background-color: #ce7b42 !important;
          border-color: #ce7b42 !important;
          color: #fff !important;
        }
        .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover, .wp-core-ui .button:focus, .wp-core-ui .button:hover {
          box-shadow: none;
        }
        a:hover, a:active { color: #e48849; }
        .flatty-top-bar-button, .flatty-top-bar a:hover, .flatty-top-bar-button, .flatty-top-bar a:active { color: #fff; }
        input, input.code { padding: 3px 5px; }
      </style>';
    }
    add_action('admin_head', 'my_custom_admin');
  // ENABLE JQUERY
    if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
    function my_jquery_enqueue() {
       wp_deregister_script('jquery');
       wp_register_script('jquery', "https" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js", false, null);
       wp_enqueue_script('jquery');
    }
  // ENABLE FEATURED IMAGE SUPPORT IN THEME
    add_theme_support('post-thumbnails');
  // ENABLE ACF OPTIONS PAGE
    if( function_exists('acf_add_options_page') ) {
      acf_add_options_page();
      acf_add_options_sub_page('Default Images');
      acf_add_options_sub_page('Theme Options');
    }
  // ALLOW SHORTCODES IN WIDGETS
    add_filter('widget_text', 'do_shortcode');
  // ALLOW SVG & ZIP FILES IN WORDPRESS
    function cc_mime_types($mimes) {
      $mimes['svg'] = 'image/svg+xml';
      $mimes['zip'] = 'application/zip';
      return $mimes;
    }
    add_filter('upload_mimes', 'cc_mime_types');
    function bodhi_svgs_disable_real_mime_check( $data, $file, $filename, $mimes ) {
      $wp_filetype = wp_check_filetype( $filename, $mimes );

      $ext = $wp_filetype['ext'];
      $type = $wp_filetype['type'];
      $proper_filename = $data['proper_filename'];

      return compact( 'ext', 'type', 'proper_filename' );
    }
    add_filter( 'wp_check_filetype_and_ext', 'bodhi_svgs_disable_real_mime_check', 10, 4 );
  // UPDATE EDITOR ROLE
    function update_editor(){
      $role = get_role('editor');
      $role->add_cap('gform_full_access');
      $role->add_cap('edit_theme_options');
      $role->add_cap('edit_users');
      $role->add_cap('list_users');
      $role->add_cap('promote_users');
      $role->add_cap('create_users');
      $role->add_cap('add_users');
      $role->add_cap('delete_users');
    }
    add_action('admin_init','update_editor');
  // UPDATE AUTHOR ROLE
    function update_author(){
      $role = get_role('author');
      $role->add_cap('gform_full_access');
      $role->add_cap('edit_posts');
      $role->add_cap('edit_others_posts');
      $role->add_cap('edit_others_posts');
    }
    add_action('admin_init','update_editor');
  // ADD SMOOTHSCROLL CLASS TO MENU LINKS WITH REL
      function add_menuclass($ulclass) {
        return preg_replace('/<a rel="smoothScroll"/', '<a rel="smoothScroll" class="smoothScroll"', $ulclass);
      }
      add_filter('wp_nav_menu','add_menuclass');
  //CONNECT RELEVANSSI TO AJAX
    function my_alm_query_args_relevanssi($args){
       $args = apply_filters('alm_relevanssi', $args);
       return $args;
    }
    add_filter( 'alm_query_args_relevanssi', 'my_alm_query_args_relevanssi');
  //ADD ACF TO BE USED IN THE RELEVANSSI CUSTOM EXCERPTS
    add_filter('relevanssi_excerpt_content', 'custom_fields_to_excerpts', 10, 3);
    function custom_fields_to_excerpts($content, $post, $query) {
        $custom_fields = get_post_custom_keys($post->ID);
        $remove_underscore_fields = true;
        if (is_array($custom_fields)) {
          $custom_fields = array_unique($custom_fields);  // no reason to index duplicates
          foreach ($custom_fields as $field) {
            if ($remove_underscore_fields) {
              if (substr($field, 0, 1) == '_') continue;
            }
            $values = get_post_meta($post->ID, $field, false);
            if ("" == $values) continue;
            foreach ($values as $value) {
              if ( !is_array ( $value ) ) {
                $content .= " | " . $value;
              }
            }
          }
        }
        return $content;
    }
  // STYLEGUIDE HEX TO RGB
    function hex2rgb( $colour ) {
      if ( $colour[0] == '#' ) { $colour = substr( $colour, 1 ); }
      if ( strlen( $colour ) == 6 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
      } elseif ( strlen( $colour ) == 3 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
      } else {
        return false;
      }
      $r = hexdec( $r ); $g = hexdec( $g ); $b = hexdec( $b );
      return 'rgb('.$r.', '.$g.', '.$b.')';
    }
  // STYLEGUIDE RGB TO CMYK
    function hex2rgb2($hex) {
      $color = str_replace('#','',$hex);
      $rgb = array(
        'r' => hexdec(substr($color,0,2)),
        'g' => hexdec(substr($color,2,2)),
        'b' => hexdec(substr($color,4,2)),
      );
      return $rgb;
    }
    function rgb2cmyk($var1,$g=0,$b=0) {
      if (is_array($var1)) { $r = $var1['r']; $g = $var1['g']; $b = $var1['b'];
      } else { $r = $var1; }
      $r = $r / 255; $g = $g / 255; $b = $b / 255;
      $bl = 1 - max(array($r,$g,$b));
      if ( ( 1 - $bl ) > 0 ) {
        $c = ( 1 - $r - $bl ) / ( 1 - $bl ); 
        $m = ( 1 - $g - $bl ) / ( 1 - $bl ); 
        $y = ( 1 - $b - $bl ) / ( 1 - $bl );
      }
      $c = round($c * 100); $m = round($m * 100); $y = round($y * 100); $bl = round($bl * 100);
      return 'cmyk('.$c.', '.$m.', '.$y.', '.$bl.')';
    }
//SETUP WORDPRESS & BACKEND


//CUSTOMIZE CONTENT FOR CLIENT
  // ADD MENUS FOR NAVIGATION
    function register_menu () {
      register_nav_menu('primary-nav', __('Primary Navigation'));
      register_nav_menu('footer-nav', __('Footer Navigation'));
      register_nav_menu('search-nav', __('Search Navigation'));
      register_nav_menu('workforce', __('Workforce'));
      register_nav_menu('behavioral', __('Behavioral'));
    }
    add_action('init', 'register_menu');
  // ADD WIDGET AREAS
    function themename_widgets_init() {
      //LEFT SIDEBAR
      register_sidebar( array(
          'name'          => __( 'Primary Sidebar', 'theme_name' ),
          'id'            => 'primary-left',
          'before_widget' => '<aside id="%1$s" class="widget %2$s">',
          'after_widget'  => '</aside>',
          'before_title'  => '<h3 class="widget-title">',
          'after_title'   => '</h3>',
      ) );
    }
    add_action( 'widgets_init', 'themename_widgets_init' );
  // PRIMARY BUTTON SHORTCODE
    function cta_button($atts, $content = null) {
      extract( shortcode_atts( array(
        'target' => '',
        'url' => '#',
        'type' => '',
        'color' => '',
      ), $atts ) );
      return '<a target="'.$target.'" href="'.$url.'" class="btn '.$color.' '.$type.'"><span>' . do_shortcode($content) . '</span></a>';
    }
    add_shortcode('cta-btn', 'cta_button');
  // LEAGAL TEXT SHORTCODE
    function legal_text($atts, $content = null) {
      extract( shortcode_atts( array( ), $atts ) );
      return '<p class="legal-text">' . do_shortcode($content) . '</p>';
    }
    add_shortcode('legal-text', 'legal_text');
  // EXCERPT LENGTH
    function get_excerpt(){
      $excerpt = get_the_excerpt();
      $excerpt = preg_replace(" ([*?])",'',$excerpt);
      $excerpt = strip_shortcodes($excerpt);
      $excerpt = strip_tags($excerpt);
      $excerpt = substr($excerpt, 0, 300);
      $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
      $excerpt = $excerpt . " ...";
      return $excerpt;
    }
  // CUSTOM POST TYPES
    function create_posttype() {
      //JOBS
      register_post_type( 'jobs',
        array(
          'labels' => array(
            'name' => __( 'Jobs' ),
            'singular_name' => __( 'Job' )
          ),
          'public' => true,
          'has_archive' => true,
          'rewrite' => array('slug' => 'jobs'),
          'taxonomies'  => array( 'category' ),
          'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
        )
      );
      //STAFF
      register_post_type( 'staff',
        array(
          'labels' => array(
            'name' => __( 'Staff' ),
            'singular_name' => __( 'Staff' )
          ),
          'public' => true,
          'has_archive' => false,
          'rewrite' => array('slug' => 'staff'),
        )
      );
      //SUCCESS STORIES
      register_post_type( 'success-stories',
        array(
          'labels' => array(
            'name' => __( 'Success Stories' ),
            'singular_name' => __( 'Success Story' )
          ),
          'public' => true,
          'has_archive' => false,
          'rewrite' => array('slug' => 'success-stories'),
        )
      );
      //HERO STORIES
      register_post_type( 'hero-stories',
        array(
          'labels' => array(
            'name' => __( 'Hero Stories' ),
            'singular_name' => __( 'Hero Story' )
          ),
          'public' => true,
          'has_archive' => false,
          'rewrite' => array('slug' => 'good-to-know'),
        )
      );
    }
    add_action( 'init', 'create_posttype' );

    //GOOGLE MAPS
    function my_theme_add_scripts() {
     wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAn1IPeyVg4Lr0Q3z__gwA684JJc5BH2Iw', array(), '3', true );
     wp_enqueue_script( 'google-map-init', get_template_directory_uri() . '/js/google-maps.js', array('google-map', 'jquery'), '0.1', true );
    }
    add_action( 'wp_enqueue_scripts', 'my_theme_add_scripts' );
    function my_acf_google_map_api( $api ){
     $api['key'] = 'AIzaSyAn1IPeyVg4Lr0Q3z__gwA684JJc5BH2Iw';
     return $api;
    }
    add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

    //CUSTOMIZE STRIPE DESCRIPTIONS
    add_filter( 'gform_stripe_charge_description', 'remove_entry_id', 10, 2 );
    function remove_entry_id( $description, $strings ) {
        unset( $strings['entry_id'] );

        $old_description = implode( ', ', $strings );;
        $new_url = str_replace("Product:","",$old_description);

        return $new_url;
    }

//CUSTOMIZE CONTENT FOR CLIENT

?>