<?php /*
THE HEADER TEMPLATE FOR OUR THEME
*/ ?>

<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php endif; ?>
  <?php wp_head(); ?>

<!-- FAVICON -->
<link rel="icon" type="image/png" href="/wp-content/uploads/2017/04/goodwill-favicon.png" />

<!-- MOBILE SITE MEDIA QUERY -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- ANIMATIONS CSS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/animate.min.css" />
<!-- SLICK SLIDER CSS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/slick.min.css" />
<!-- FLUIDBOX CSS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/fluidbox.min.css" />
<!-- LITY CSS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/lity.min.css" />
<!-- MAIN CSS -->
  <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.beautified.min.css" />
</head>

<body <?php body_class('font-size-one'); ?>>

<!-- SITE NAVIGATION -->
  <?php get_template_part( 'template-parts/content', 'primary-nav' ); ?>