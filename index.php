<?php /* 
MAIN BLOG PAGE 
*/ ?>

<?php get_header(); ?>

<main class="page-contents-container full-width grey-bg">

    <!-- PAGE TITLE -->
    <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

    <!--SKIP NAV -->
    <a id="main-anchor" class="page-anchor"></a>

    <!-- SECONDARY NAVIGATION -->
    <?php get_template_part( 'template-parts/content', 'secondary-nav' ); ?>

    <?php 
      $orderby = $_GET["orderby"];
      $order = $_GET["order"];
    ?>

    <section class="blog-feed post-feed max-width">
      <?php if ( $order && $orderby ) { ?>
        <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" order="'.$order.'" orderby="'.$orderby.'" posts_per_page="6" scroll="false" button_label="Load More"]') ?>
      <?php } else { ?>
        <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="6" scroll="false" button_label="Load More"]') ?>
      <?php } ?>
    </section>

  <!-- BACK TO TOP -->
  <a href="#main-anchor" class="back-to-top skip smoothScroll hidden">
    <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
      <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
    </svg>
    <span>Back to Top</span>
  </a>

</main>

<?php get_footer(); ?>