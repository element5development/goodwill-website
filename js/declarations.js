/* DECLARATIONS FOR ANIMATIONS, STICKY PORTIONS, SLIDERS, ETC. | ONLY MINIFIED VERSIONS ARE CALLED IN THEME */


/*BACK TO TOP ENTRANCE*/
jQuery(document).ready(function() {
  jQuery(window).scroll(function() {    
      var scroll = $(window).scrollTop();

      if (scroll >= 2500) {
          $(".back-to-top").addClass("visible animated fadeIn");
      } else {
          $(".back-to-top").removeClass("visible animated fadeIn");
      }
  });
});

/*INCREASE FONT SIZE*/
$(function() {
  var min = 16;
  var max = 20;

  if( Cookies.get('fontSize') >= 0 ) {
      fontSize = parseInt(Cookies.get('fontSize'));
      jQuery('body').css('font-size',fontSize);
  } else {
      fontSize = 16;
  }

  Cookies.set('fontSize', fontSize , { expires: 365, path: '/' });

  jQuery("#font-increase").on('click', function(event) {
      event.preventDefault();
      if(fontSize < max ) {
          fontSize = fontSize + 2;
      } else {
        fontSize = 16;
      }
      Cookies.set('fontSize',fontSize);
      jQuery('body').css('font-size',fontSize);
  });

  if (fontSize) {
    $('body').css('font-size', fontSize + 'px');
  }
})

/*SEARCH OPEN & CLOSE*/
$('#open-search').on('click', function(e){
    $('#search-open').addClass('open');
});
$('#search-close').on('click', function(e){
    $('#search-open').removeClass('open');
});
$('.search-container  *')
.focus(function() {
    $('.search-container').addClass('open');
})
.blur(function() {
    $('.search-container').removeClass('open');
});

/*MENU OPEN & CLOSE*/
$('#mobile-menu').on('click', function(e){
  if ( $(this).hasClass('close') ) {
    $('.mobile-menu').removeClass('close');
    $('.primary-nav nav').removeClass('open');
  } else {
    $('.mobile-menu').addClass('close');
    $('.primary-nav nav').addClass('open');
  }
});

/*SECONDARY NAV*/
$('.container-title').on('click', function(e){
    $(this).parent().toggleClass('open');
});
$('.secondary-nav.blog-nav > ul > li').on('click', function(e){
    $('.secondary-nav.blog-nav > ul > li').removeClass('open');
    $(this).toggleClass('open');
});

/*FLIP THE SCRIPT CARDS*/
$('.read-more').on('click', function(e){
    $(this).parent().toggleClass('open');
});

/*OVERLAY CARDS*/
$('.card-overlay').on('click', function(e){
  if ( $(this).hasClass('open')) {
    $('.card-overlay').removeClass('open');
  } else {
    $('.card-overlay').removeClass('open');
    $(this).addClass('open');
  }
});

/*SLICK SLIDERS*/
$('.slider-container').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 750,
    arrows: false,
    dots: true,
    fade: true,
    cssEase: 'linear'
});
$('.speaker-slider-container').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
});

/*FLUIDBOX*/
$(function () {
    $('a.fluidbox').fluidbox();
});

/*MASONRY*/
$('.gallery-container').masonry({
  itemSelector: '.gallery-item',
  columnWidth: '.grid-sizer',
  percentPosition: true
});

/*CUSTOM RADIO BUTTONS*/
$(document).ready(function() {
    var $radioButtons = $('input[type="radio"]');
    $radioButtons.click(function() {
        $radioButtons.each(function() {
            $(this).parent().toggleClass('checkedd', this.checked);
        });
    });
});

/*TWEET THIS AFTER BLOCKQUOTES*/
var twshare = "https://twitter.com/share?url=''";
$("blockquote").each(function () {
    var $link = $("<a class='tweet' />"),
        linkText = "Tweet this",
        url = twshare + "&text=" + $(this).text() + " @Goodwill";
    $link.attr("href", url);
    $link.attr("title", "Tweet this quote");
    $link.attr("target", "_blank");
    $link.text(linkText);
    $link.addClass('tweet-this');
    $(this).append($link);
});

/*PARALAX PAGE TITLES*/
jQuery(window).scroll(function () {
    jQuery(".page-title-container ").css("background-position","center top " + ( (jQuery(this).scrollTop() / 30) + 50 ) + "%");
});


/* STICKY PORTIONS*/
jQuery(document).ready(function() {
    var window_width = jQuery(window).width();
    if (window_width < 750) {
        //DISABLE STICKY PARTS ON MOBILE
        jQuery("#sticky-nav").trigger("sticky_kit:detach");
        jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
    } else {
        //ENABLE STICKY PARTS
        jQuery("#sticky-nav").stick_in_parent({
            parent: "body"
        });
        jQuery("#styleguide-sidebar").stick_in_parent({
            offset_top: 125
        });
    }
    jQuery(window).resize(function() {
        window_width = jQuery(window).width();
        if (window_width < 750) {
            //DISABLE STICKY PARTS ON MOBILE
            jQuery("#sticky-nav").trigger("sticky_kit:detach");
            jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
        } else {
            //ENABLE STICKY PARTS
            jQuery("#sticky-nav").stick_in_parent({
                parent: "body"
            });
            jQuery("#styleguide-sidebar").stick_in_parent({
                offset_top: 125
            });
        }
    });
});

/*COUNT UP*/
jQuery(document).ready(function() {
    jQuery(".counter").viewportChecker({
        classToAdd: "count",
        offset: 100
    });
});
jQuery(document).ready(function() {
    $(window).scroll(function() {   
        $('.count').each(function() {
          var $this = $(this),
              countTo = $this.attr('data-count');
          $({ countNum: $this.text()}).animate({
            countNum: countTo
          },
          {
            duration: 1000,
            easing:'linear',
            step: function() {
              $this.text(Math.floor(this.countNum));
            },
            complete: function() {
              $this.text(this.countNum);
              //alert('finished');
            }

          });  
        });
    });
});

/* FILE UPLOAD */
jQuery(function($) {
  $('input[type="file"]').change(function() {
    if ($(this).val()) {
      error = false;
      var filename = $(this).val();
      var filnameShort = filename.substring(12, 32) + "..."; 
      $('#extensions_message_6_7').html(filnameShort);
      if (error) {
        parent.addClass('error').prepend.after('<div class="alert alert-error">' + error + '</div>');
      }
    }
  });
});

/*SKIP NAV*/
$( document ).ready(function() {
  $(".skip").click(function(event){
      var skipTo="#"+this.href.split('#')[1];
      $(skipTo).attr('tabindex', -1).on('blur focusout', function () {
          $(this).removeAttr('tabindex');
      }).focus();
  });
});

/*DROPDOWNS ON FOCUS*/
$('.services-nav .workforce-nav ul.menu li a').bind('blur', function(){ $('.workforce-nav').removeClass("open"); });
$('.services-nav .workforce-nav ul.menu li a').bind('focus', function(){ $('.workforce-nav').addClass("open"); });
$('.services-nav .behaviioral-nav ul.menu li a').bind('blur', function(){ $('.behaviioral-nav').removeClass("open"); });
$('.services-nav .behaviioral-nav ul.menu li a').bind('focus', function(){ $('.behaviioral-nav').addClass("open"); });
$('.secondary-nav.blog-nav ul ul li a').bind('blur', function(){ $('.secondary-nav.blog-nav ul li').removeClass("open"); });
$('.secondary-nav.blog-nav ul ul li a').bind('focus', function(){ $('.secondary-nav.blog-nav ul li').addClass("open"); });