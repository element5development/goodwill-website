<?php /*
DEFAULT PAGE TEMPLATE
*/ ?>

<?php get_header(); ?>

<main class="page-contents-container full-width">

  <!-- PAGE TITLE -->
  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <!--SKIP NAV -->
  <a id="main-anchor" class="page-anchor"></a>

    <!-- SECONDARY NAVIGATION -->
    <?php get_template_part( 'template-parts/content', 'secondary-nav' ); ?>

    <!-- MISSION -->
    <?php if ( is_page(390) ) { //ABOUT ?>
      <?php get_template_part( 'template-parts/content', 'mission' ); ?>
    <?php } ?>

    <!-- GREENWORKS SLIDER -->
    <?php if( have_rows('greenworks_slides') ) { ?>
      <?php get_template_part( 'template-parts/content', 'slider-greenworks' ); ?>
    <?php } ?>

  	<!-- ADD PAGE CONTENT -->
  	<?php if ( get_the_content() ) { ?>
      <div class="editor-contents <?php if ( !is_page(505) ) { ?>max-width<?php } ?>">
  		  <?php the_content(); ?>
      </div>
    <?php } ?>

    <!-- DONATION CTA -->
    <?php if ( is_page(395) ) { //DONATE ?>
      <?php get_template_part( 'template-parts/content', 'cta-donation' ); ?>
    <?php } ?>

    <!-- GALLERY -->
    <?php if ( get_field('page_gallery') ) { ?> 
      <?php get_template_part( 'template-parts/content', 'page-gallery' ); ?>
    <?php } ?>

    <!-- AUTOMOTIVE PDFS -->
    <?php if( have_rows('pdfs') ) { ?>
      <?php get_template_part( 'template-parts/content', 'auto-pdfs' ); ?>
    <?php } ?>

    <!-- AUTOMOTIVE SLIDER -->
    <?php if( have_rows('auto_slides') ) { ?>
      <?php get_template_part( 'template-parts/content', 'slider-auto' ); ?>
    <?php } ?>

    <!-- CONTENT WITH BACKGROUND IMAGE -->
    <?php if ( get_field('content_background_image') ) { //ABOUT ?>
      <?php get_template_part( 'template-parts/content', 'content-bg' ); ?>
    <?php } ?>

    <!-- FOR BUSINESSES CONTACT -->
    <?php if ( is_page('508') ) { ?>
      <div class="max-width">
        <h2 style="text-align: center;">Contact Us About Our Services</h2>
        <?php echo do_shortcode('[gravityform id="7" title="false" description="false"]'); ?>
      </div>
    <?php } ?>

    <!-- HELP -->
    <?php if ( get_field('help_left') && get_field('help_right') ) { ?>
      <?php get_template_part( 'template-parts/content', 'help' ); ?>
    <?php } ?>

    <!-- RETAIL LOCATIONS -->
    <?php if( have_rows('locations') ) { ?>
      <?php get_template_part( 'template-parts/content', 'retail-locations' ); ?>
    <?php } ?>

    <!-- WHAT WE ACCEPT -->
    <?php if ( get_field('we_accept') && get_field('we_cannot_accept') ) { ?>
      <?php get_template_part( 'template-parts/content', 'donate-accept' ); ?>
    <?php } ?>

    <!-- ROI -->
    <?php if ( is_page(390) ) { //ABOUT ?>
      <?php get_template_part( 'template-parts/content', 'roi' ); ?>
    <?php } ?>

    <!-- STAFF -->
    <?php if ( is_page(390) ) { //ABOUT ?>
      <?php get_template_part( 'template-parts/content', 'staff' ); ?>
    <?php } ?>

    <!-- 990 CTA -->
    <?php if ( is_page(390) ) { //ABOUT ?>
      <?php get_template_part( 'template-parts/content', 'cta-990' ); ?>
    <?php } ?>

    <!-- CARDS ICON -->
    <?php if( have_rows('icon_cards') ) { ?>
      <?php get_template_part( 'template-parts/content', 'cards-icon' ); ?>
    <?php } ?>

    <!-- CARDS CTA -->
    <?php if ( have_rows('cta_cards') ) { ?>
      <?php get_template_part( 'template-parts/content', 'cards-cta' ); ?>
    <?php } ?>

    <!-- CARDS FLIP -->
    <?php if ( have_rows('flip_cards')) { ?>
      <?php get_template_part( 'template-parts/content', 'cards-flip' ); ?>
    <?php } ?>

    <!-- DONATION CTA -->
    <?php if ( is_front_page() || is_page(395) ) { //HOME || DONATE ?>
      <?php get_template_part( 'template-parts/content', 'cta-donation' ); ?>
    <?php } ?>

    <!-- CONTACT FORM -->
    <?php if ( get_field('contact_form') && get_field('contact_map') ) { //GREENWORKS || AUTOMOTIVE ?> 
      <?php get_template_part( 'template-parts/content', 'service-contact' ); ?>
    <?php } ?>

    <!-- WHAT WE ACCEPT / ACCOUNTABILITY -->
    <?php if( have_rows('organizations') ) { ?>
      <?php get_template_part( 'template-parts/content', 'other-support' ); ?>
    <?php } ?>

    <!-- EVENTS QUERY -->
    <?php if ( is_front_page() ) { ?>
      <?php get_template_part( 'template-parts/content', 'events' ); ?>
    <?php } ?>

    <!-- SUCCESS STORIES QUERY -->
    <?php if ( !is_page(390) && !is_page(391) && !is_page(395) && !is_page(505) && !is_page(949) ) { ?>
      <?php get_template_part( 'template-parts/content', 'success-stories' ); ?>
    <?php } ?>

    <!-- BACK TO TOP -->
    <a href="#main-anchor" class="back-to-top skip smoothScroll hidden">
      <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
        <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
      </svg>
      <span>Back to Top</span>
    </a>

</main>

<?php get_footer(); ?>