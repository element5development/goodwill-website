<?php /*
THE CONTAINER FOR SEARCH RESULTS PAGES
*/ ?>


<?php get_header(); ?>

<main class="page-contents-container full-width grey-bg">

    <!-- PAGE TITLE -->
    <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

    <!--SKIP NAV -->
    <a id="main-anchor" class="page-anchor"></a>

    <div class="popular-links max-width">
	    <h3>Didn't find what you were looking for? Try one of these popular pages.</h3>
	    <nav class="secondary-nav max-width">
        <?php wp_nav_menu( array( 'theme_location' => 'search-nav' ) ); ?>
      </nav> 
    </div>

    <?php $term = (isset($_GET['s'])) ? $_GET['s'] : ''; // Get 's' querystring param ?>
    <section class="blog-feed post-feed max-width">
       <?php echo do_shortcode('[ajax_load_more id="relevanssi" search="'. $term .'" post_type="post, page, facebook_events, jobs, staff, hero-stories" posts_per_page="6" scroll="false" button_label="Load More" id="relevanssi"]'); ?>
    </section>


</main>

<?php get_footer(); ?>