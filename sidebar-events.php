<?php /*
DISPLAY SERVICES SIDE NAVIGATION
*/ ?>

<!-- NEWSLETTER -->
<div class="newsletter-widget dark-bg">
  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/newsletter-graphic.png" width="50" height="50" alt="newsletter"/>
  <h2><span>Stay</span> Up-to-Date</h2>
  <p>Be the first to know about our local events and volunteer opportunities.</p>
  <?php echo do_shortcode('[gravityform id="5" title="false" description="false"]'); ?>
</div>