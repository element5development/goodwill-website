<?php /*
THE TEMPLATE FOR DISPLAYING SINGLE JOB POST
*/ ?>

<?php get_header(); ?>

<main class="page-contents-container full-width">

  <!-- PAGE TITLE -->
  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <!--SKIP NAV -->
  <a id="main-anchor" class="page-anchor"></a>

  <!-- JOB DETAILES -->
  <div class="job-details flex-container max-width">
    <div class="one-half">
        <h2>Working at Goodwill</h2>
        <a href="#application" class="smoothScroll apply-below">Apply Below<svg xmlns="http://www.w3.org/2000/svg" width="8" height="11" viewBox="0 0 8 11"><path fill="#a5cb5a" d="M0 10.28v-10.28l7.96 5.47z"/></svg></a>
        <p>
          Goodwill Industries of Greater Detroit<br/>
          Human Resources Department
          3111 Grand River Ave., Detroit, MI 48208
        </p>
        <p>For more information, please contact<br/>Human Resources:</p>
        <a href="tel:+1-313-557-8766" class="btn primry-btn phone"><span>Call 313-557-8766</span></a><a href="mailto:jobs@goodwilldetroit.org" class="btn primry-btn email"><span>Email</span></a>
    </div>
  </div>

  <!-- ADD PAGE CONTENT -->
  <?php if ( get_the_content() ) { ?>
    <div class="editor-contents <?php if ( !is_page(505) ) { ?>max-width<?php } ?>">
      <h3><?php the_title(); ?></h3>
      <?php the_content(); ?>
    </div>
  <?php } ?>

  <div class="application grey-bg">
    <a id="application" class="page-anchor"></a>
    <div class="max-width">
      <h2>Apply</h2>
      <?php 
        if ( get_field('job_application') == 'Full' ) { 
          echo do_shortcode('[gravityform id="6" title="false" description="false"]'); 
        } else {
          echo do_shortcode('[gravityform id="10" title="false" description="false"]'); 
        } 
      ?>
    </div>
  </div>


  <!-- BACK TO TOP -->
  <a href="#main-anchor" class="back-to-top skip smoothScroll hidden">
    <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
      <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
    </svg>
    <span>Back to Top</span>
  </a>

</main>

<?php get_footer(); ?>