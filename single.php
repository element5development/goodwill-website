<?php /*
THE TEMPLATE FOR DISPLAYING SINGLE BLOG POST
*/ ?>

<?php get_header(); ?>

<main class="page-contents-container full-width">

  <!-- PAGE TITLE -->
  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <!--SKIP NAV -->
  <a id="main-anchor" class="page-anchor"></a>

  <!-- ADD PAGE CONTENT -->
  <?php if ( get_the_content() ) { ?>
    <div class="editor-contents max-width">
      <?php the_content(); ?>
    </div>
  <?php } ?>

  <!-- SOCIAL SHARE -->
  <div class="share-post vertical-align-parent grey-bg">
    <div class="vertical-align-child">
      <h2>Did this story inspire you?</h2>
      <p>PLEASE SHARE IT.</p>
      <div class="social-links social-share">
        <?php 
          $link = urlencode(get_permalink());
          $post_title = str_replace( ' ', '%20', get_the_title());
        ?>
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" target="_blank" class="facebook social-link">
          <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.113 430.114" enable-background="new 0 0 430.113 430.114">
            <path d="M158.081 83.3v59.218h-43.385v72.412h43.385v215.183h89.122v-215.177h59.805s5.601-34.721 8.316-72.685h-67.784v-49.511c0-7.4 9.717-17.354 19.321-17.354h48.557v-75.385h-66.021c-93.519-.005-91.316 72.479-91.316 83.299z"></path>
          </svg>
        </a>
        <a href="https://twitter.com/intent/tweet?text=<?php echo $post_title; ?>&amp;url=<?php echo $link; ?>" target="_blank" class="twitter social-link">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612" width="512" height="512" enable-background="new 0 0 612 612">
            <path d="M612 116.258c-22.525 9.981-46.694 16.75-72.088 19.772 25.929-15.527 45.777-40.155 55.184-69.411-24.322 14.379-51.169 24.82-79.775 30.48-22.907-24.437-55.49-39.658-91.63-39.658-69.334 0-125.551 56.217-125.551 125.513 0 9.828 1.109 19.427 3.251 28.606-104.326-5.24-196.835-55.223-258.75-131.174-10.823 18.51-16.98 40.078-16.98 63.101 0 43.559 22.181 81.993 55.835 104.479-20.575-.688-39.926-6.348-56.867-15.756v1.568c0 60.806 43.291 111.554 100.693 123.104-10.517 2.83-21.607 4.398-33.08 4.398-8.107 0-15.947-.803-23.634-2.333 15.985 49.907 62.336 86.199 117.253 87.194-42.947 33.654-97.099 53.655-155.916 53.655-10.134 0-20.116-.612-29.944-1.721 55.567 35.681 121.536 56.485 192.438 56.485 230.948 0 357.188-191.291 357.188-357.188l-.421-16.253c24.666-17.593 46.005-39.697 62.794-64.861z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $link; ?>&amp;title=<?php echo $post_title; ?>" target="_blank" class="linkedin social-link">
          <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.117 430.117" enable-background="new 0 0 430.117 430.117">
            <path d="M430.117 261.543v159.017h-92.188v-148.367c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477v154.873h-92.219s1.242-251.285 0-277.32h92.21v39.309l-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zm-377.934-251.985c-31.548 0-52.183 20.693-52.183 47.905 0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zm-46.706 411.002h92.184v-277.32h-92.184v277.32z"></path>
          </svg>
        </a>
        <a href="mailto:?Subject=Hey, Check this out!&amp;Body=Check%20out%20<?php the_title(); ?>%20story:%20<?php the_permalink(); ?>" class="email social-link">
          <svg xmlns="http://www.w3.org/2000/svg" width="43" height="29" viewBox="0 0 43 29">
            <path d="M15.72 15.22l-15.1 13.22c.36.32.84.54 1.36.54h38.2c.52 0 1-.22 1.34-.54l-15.08-13.22-5.36 5.2zm-2.3-2.22l-12.84-12.42c-.36.34-.58.84-.58 1.4v25.02c0 .52.2 1 .54 1.34zm7.66 3.46l20.42-15.94c-.36-.32-.82-.52-1.32-.52h-38.2c-.52 0-.98.2-1.32.52zm7.64-3.46l12.84-12.42c.36.34.58.84.58 1.4v25.02c0 .52-.2 1-.52 1.34z"></path>
          </svg>
        </a>
        <a href="javascript:;" onclick="window.print()" class="print social-link">
          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
            <path d="M13.46 33.64h12.22v1.84h-12.22zm0-3.68h12.22v1.84h-12.22zm0-3.66h12.22v1.84h-12.22zm15.9-3.68h-19.58v14.68c0 1.02.82 1.84 1.84 1.84h15.9c1.02 0 1.84-.82 1.84-1.84zm0-20.8c0-.38-.12-.74-.32-1.02l-.02-.02c-.06-.08-.12-.18-.2-.24l-.02-.02c-.1-.1-.22-.18-.34-.26-.18-.1-.36-.18-.56-.22l-.26-.02-.12-.02h-15.9c-.76 0-1.42.46-1.68 1.12-.1.22-.16.46-.16.72v5.5h19.58zm-25.7 12.86c0 1.02.84 1.84 1.84 1.84 1.02 0 1.84-.82 1.84-1.84s-.82-1.84-1.84-1.84c-1 0-1.84.82-1.84 1.84zm27.54 15.28v-9.16h-23.24v9.16h-6.12c-1.02 0-1.84-.82-1.84-1.82v-17.14c0-1 .82-1.82 1.84-1.82h35.46c1.02 0 1.84.82 1.84 1.82v17.14c0 1-.82 1.82-1.84 1.82z"></path>
          </svg>
        </a>
      </div>  
    </div>
  </div>

  <?php comments_template(); ?> 

  <!-- BACK TO TOP -->
  <a href="#main-anchor" class="back-to-top skip smoothScroll hidden">
    <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
      <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
    </svg>
    <span>Back to Top</span>
  </a>

</main>

<?php get_footer(); ?>