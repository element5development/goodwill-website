<?php /*
DISPLAY PDF DOWNLOAD LINKS
*/ ?>

<?php if( have_rows('pdfs') ) { ?>
  <div class="auto-pdfs dark-bg">
    <div class="max-width">
      <h3>If you are already a client of Goodwill Automotive, here are a few things that may be helpful:</h3>
      <div class="flex-container">
        <?php while ( have_rows('pdfs') ) : the_row(); ?>

          <a href="<?php the_sub_field('pdf_url'); ?>" target="_blank" class="hero-preview card-post post-preview grey-bg one-fourth">
            <?php $image = get_sub_field('pdf_image'); ?>
            <div class="post-top" style="background-image: url('<?php echo $image['sizes']['medium']; ?>');"></div>
            <div class="post-bottom">
              <h4><?php the_sub_field('pdf_text'); ?></h4>
              <div class="learn-more">Download
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                  <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                </svg>
              </div>
            </div>
          </a>

        <?php endwhile; ?>
      </div>
    </div>
  </div>
<?php } else {
    // no rows found
} ?>
