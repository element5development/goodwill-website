<?php /*
DISPLAY CTA CARD REPEATER
*/ ?>



<?php if( have_rows('cta_cards') ) { ?>
  <section class="card-container full-width">
    <div class="max-width">
      <?php if ( get_field('cta_card_section_title') ) { ?>
        <h2><?php the_field('cta_card_section_title') ?></h2>
      <?php } ?>
      <?php if ( get_field('cta_card_section_subheading') ) { ?>
        <p class="section_subheading"><?php the_field('cta_card_section_subheading') ?></p>
      <?php } ?>
    </div>
    <div class="max-width flex-container">
      <?php while ( have_rows('cta_cards') ) : the_row(); ?>

        <?php $image = get_sub_field('card_image'); ?>
        <?php $icon = get_sub_field('card_icon'); ?>

        <a href="<?php the_sub_field('card_link') ?>" class="card-cta one-third" style="background-image: url('<?php echo $image['url']; ?>');">
          <div class="card-contents">
            <div class="card-icon-label vertical-align-parent">
              <div class="vertical-align-child"><img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>"/></div>
            </div>
            <h3><?php the_sub_field('card_title') ?></h3>
          </div>
        </a>

      <?php endwhile; ?>
    </div>
  </section>
<?php } else { 
    // NO SLIDES
} ?>