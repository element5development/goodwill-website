<?php /*
DISPLAY FLIP CARD REPEATER
*/ ?>



<?php if( have_rows('flip_cards') ) { ?>

  <section class="card-container full-width">
    <a id="accountability" class="page-anchor"></a>
    <div class="card-container flip-card-title-container grey-bg">
      <?php if ( get_field('flip_card_section_title') ) { ?>
        <h2><?php the_field('flip_card_section_title') ?></h2>
      <?php } ?>
      <?php if ( get_field('flip_card_section_subheading') ) { ?>
        <p class="section_subheading"><?php the_field('flip_card_section_subheading') ?></p>
      <?php } ?>
    </div>
    <div class="card-container flip-card-container">
      <div class="max-width flex-container">
        <?php while ( have_rows('flip_cards') ) : the_row(); ?>

          <a href="<?php the_sub_field('card_link') ?>" class="flip-container card-flip one-third">
            <div class="flipper">
              <div class="front">
                <?php $image = get_sub_field('card_image'); ?>
                <div class="card-img" style="background-image: url('<?php echo $image['url']; ?>');"></div>
                <h3><?php the_sub_field('card_title') ?></h3>
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                  <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                </svg>
              </div>
              <div class="back">
                <h3><?php the_sub_field('card_back_title') ?></h3>
                <p><?php the_sub_field('card_back_content') ?></p>
                <div class="learn-more btn primary-btn icon"><span>Learn More</span></div>
              </div>
            </div>
          </a>

        <?php endwhile; ?>
      </div>
    </div>
  </section>
<?php } else { 
    // NO SLIDES
} ?>