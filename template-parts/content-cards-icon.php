<?php /*
DISPLAY ICON CARD REPEATER
*/ ?>



<?php if( have_rows('icon_cards') ) { ?>
  <section class="card-container flip-card-container grey-bg full-width">
    <div class="max-width flex-container">
      <?php while ( have_rows('icon_cards') ) : the_row(); ?>

          <a href="<?php the_sub_field('card_link') ?>" class="card-icon one-fourth">
            <?php $image = get_sub_field('card_icon'); ?>
            <div class="card-icon-label" style="background-image: url('<?php echo $image['url']; ?>');"></div>
            <h2><?php the_sub_field('card_title') ?></h2>
            <div class="learn-more">Learn More
              <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
              </svg>
            </div>
        </a>

      <?php endwhile; ?>
    </div>
  </section>
<?php } else { 
    // NO SLIDES
} ?>