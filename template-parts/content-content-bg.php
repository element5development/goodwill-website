<?php /*
DISPLAY CONTENT WITH IMAGE BACKGROUND
*/ ?>

<?php $image = get_field('content_background_image'); ?>
<div class="content-with-bg vertical-align-parent dark-bg parallax" style="background-image: url('<?php echo $image['url']; ?>')">
  <div class="contents vertical-align-child">
    <?php the_field('content_with_bg'); ?>
  </div>
  <div class="dark-overlay"></div>
</div>
