<?php /*
DISPLAY CTA TO COLLECT DONATIONS
*/ ?>

<section class="donation-cta download-cta dark-bg full-width">
  <div class="max-width vertical-align-parent">
    <div class="vertical-align-child">
      <h2>Download our<br/>Form 990</h2>
      <a href="<?php the_field('990_download_url'); ?>" target="_blank" class="btn primary-btn icon"><span>Download</span></a>
    </div>
  </div>
</section>
