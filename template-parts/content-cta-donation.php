<?php /*
DISPLAY CTA TO COLLECT DONATIONS
*/ ?>

<section class="donation-cta dark-bg full-width">
  <div class="max-width vertical-align-parent">
    <div class="vertical-align-child">
      <h2>Donate Now.</h2>
      <a href="<?php echo get_permalink(949); ?>" class="btn primary-btn icon"><span>Give</span></a>
    </div>
  </div>
</section>
