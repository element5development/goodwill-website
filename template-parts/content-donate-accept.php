<?php /*
DISPLAY 2 BOXES SIDE BY SIDE WITH BASIC EDITOR OPTIONS
*/ ?>


<section class="help-container accept-container">
  <a id="donate-what" class="page-anchor"></a>
  <div class="section-title-container">
    <?php if ( get_field('donate_what_section_title') ) { ?>
      <h2><?php the_field('donate_what_section_title') ?></h2>
    <?php } ?>
    <?php if ( get_field('donate_what_section_subheading') ) { ?>
      <p><?php the_field('donate_what_section_subheading') ?></p>
    <?php } ?>
  </div>
  <div class="flex-container max-width">
    <div class="one-half grey-bg">
      <svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewBox="0 0 42 42">
        <path d="M29.9 16.93c.35-.35.57-.84.57-1.37 0-1.08-.87-1.95-1.95-1.95-.53 0-1.02.22-1.37.57l-9 9-4.46-4.46c-.35-.35-.84-.57-1.37-.57-1.08 0-1.95.87-1.95 1.95 0 .53.22 1.02.57 1.37l5.84 5.84c.35.35.83.56 1.37.56.54 0 1.02-.21 1.38-.56zm-9.16-13.04c9.31 0 16.86 7.55 16.86 16.85 0 9.31-7.55 16.86-16.86 16.86-9.3 0-16.85-7.55-16.85-16.86 0-9.3 7.55-16.85 16.85-16.85zm0 37.6c11.46 0 20.75-9.29 20.75-20.75 0-11.45-9.29-20.74-20.75-20.74-11.45 0-20.74 9.29-20.74 20.74 0 11.46 9.29 20.75 20.74 20.75z" fill="#a5cb5a"></path>
      </svg>
      <h3>What We Can Accept</h3>
      <?php the_field('we_accept'); ?>
    </div>
    <div class="one-half grey-bg">
      <svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewBox="0 0 42 42"> 
        <path d="M26.57 29.32l-5.77-5.77-5.77 5.77c-.36.36-.84.58-1.38.58-1.08 0-1.95-.88-1.95-1.95 0-.54.22-1.03.57-1.38l5.77-5.77-5.77-5.77c-.35-.36-.57-.84-.57-1.38 0-1.08.87-1.95 1.95-1.95.54 0 1.02.22 1.38.57l5.77 5.77 5.77-5.77c.35-.35.84-.57 1.38-.57 1.07 0 1.95.87 1.95 1.95 0 .54-.22 1.02-.58 1.38l-5.77 5.77 5.77 5.77c.36.35.58.84.58 1.38 0 1.07-.88 1.95-1.95 1.95-.54 0-1.03-.22-1.38-.58zm-22.67-8.52c0 9.33 7.56 16.89 16.9 16.89 9.33 0 16.89-7.56 16.89-16.89 0-9.34-7.56-16.9-16.89-16.9-9.34 0-16.9 7.56-16.9 16.9zm-3.9 0c0-11.49 9.31-20.8 20.8-20.8 11.48 0 20.79 9.31 20.79 20.8 0 11.48-9.31 20.79-20.79 20.79-11.49 0-20.8-9.31-20.8-20.79z" fill="#c67172"></path>
      </svg>
      <h3>What We Cannot Accept</h3>
      <?php the_field('we_cannot_accept'); ?>
    </div>
  </div>
</section>