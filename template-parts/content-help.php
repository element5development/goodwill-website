<?php /*
DISPLAY 2 BOXES SIDE BY SIDE WITH BASIC EDITOR OPTIONS
*/ ?>


<section class="help-container">
  <a id="donate-to" class="page-anchor"></a>
  <div class="section-title-container">
    <?php if ( get_field('help_section_title') ) { ?>
      <h2><?php the_field('help_section_title') ?></h2>
    <?php } ?>
    <?php if ( get_field('help_section_subheading') ) { ?>
      <h6><?php the_field('help_section_subheading') ?></h6>
    <?php } ?>
  </div>
  <div class="flex-container max-width">
    <div class="one-half grey-bg">
      <?php the_field('help_left'); ?>
    </div>
    <div class="one-half grey-bg">
      <?php the_field('help_right'); ?>
    </div>
  </div>
</section>