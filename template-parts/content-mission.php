<?php /*
DISPLAY MISSION: PHOTO, QUOTE, TEXT, SLIDER GALLERY
*/ ?>



<section class="mission-container">
	<section class="about-video-section">
		<div class="video-wrapper">
			<?php the_field('about_video'); ?>
		</div>
	</section>
  <div class="mission flex-container max-width">
    <a id="mission" class="page-anchor"></a>
    <div class="one-half mission-img">
      <?php 
        $image = get_field('mission_image'); 
      ?>
      <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
    </div>
    <div class="one-half">
      <h2>Our Mission</h2>
      <blockquote><p><?php the_field('mission_quote'); ?></p></blockquote>
    </div>
  </div>
  <div class="history grey-bg">
    <a id="history" class="page-anchor"></a>
    <div class="flex-container max-width">
      <div class="one-half">
        <?php the_field('history_content'); ?>
      </div>
      <div class="one-half">
      <?php $images = get_field('history_gallery_slider'); ?>
        <div class="slider-container full-width dark-bg">
          <?php foreach( $images as $image ): ?>
            <div class="single-slide" style="background-image: url('<?php echo $image['sizes']['medium']; ?>');"></div>
           <?php endforeach; ?>
        </div>

      </div>
    </div>
  </div>
   <?php $image = get_field('history_background'); ?>
  <div class="history-bg dark-bg parallax" style="background-image: url('<?php echo $image['url']; ?>')">
    <div class="history-closing">
      <?php the_field('history_closing'); ?>
    </div>
    <div class="dark-overlay"></div>
  </div>
</section>