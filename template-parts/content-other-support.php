<?php /*
DISPLAY ACF REPATER OF LOGOS AND LINKS TO OUTSIDE SOURCES
*/ ?>


<section class="other-support-container">
  <a id="other-ways" class="page-anchor"></a>
  <a id="accountability" class="page-anchor"></a>
  <div class="section-title-container">
    <?php if ( get_field('other_support_title') ) { ?>
      <h2><?php the_field('other_support_title') ?></h2>
    <?php } ?>
    <?php if ( get_field('other_support_subheading') ) { ?>
      <p><?php the_field('other_support_subheading') ?></p>
    <?php } ?>
  </div>
  <div class="flex-container max-width">
    <?php if( have_rows('organizations') ) {
      while ( have_rows('organizations') ) : the_row(); ?>

        <a href="<?php the_sub_field('link'); ?>" target="_blank" class="one-third grey-bg">
          <div class="vertical-align-parent">
            <div class="vertical-align-child">
              <?php $image = get_sub_field('organization_logo'); ?>
              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
          </div>
        </a>

      <?php endwhile;
    } else {
      // no rows found
    } ?>
  </div>
</section>