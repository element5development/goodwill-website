<?php /*
FOOTER CONTENT
*/ ?>

<?php if (  is_page_template( 'template-styleguide.php' ) ) { ?>
  <footer id="footer" class="dark-bg">
    <div class="copyright-container">
      <div class="max-width">
        <p id="copyright">©Copyright <?php echo date('Y'); ?> Goodwill. All Rights Reserved.</p> 
        <p id="e5-credit">Crafted by <a href="https://element5digital.com/">Element5</a></p>
      </div>
    </div>
  </footer>
<?php } else { ?>
  <footer id="footer" class="dark-bg">
    <div class="follow-container">
      <div class="social-links max-width">
        <a href="<?php the_field('facebook-url','options') ?>" target="_blank" class="facebook social-link">
          <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.113 430.114" enable-background="new 0 0 430.113 430.114">
            <path d="M158.081 83.3v59.218h-43.385v72.412h43.385v215.183h89.122v-215.177h59.805s5.601-34.721 8.316-72.685h-67.784v-49.511c0-7.4 9.717-17.354 19.321-17.354h48.557v-75.385h-66.021c-93.519-.005-91.316 72.479-91.316 83.299z"></path>
          </svg>
          <span>Goodwill Detroit Facebook</span>
        </a>
        <a href="<?php the_field('instagram-url','options') ?>" target="_blank" class="instagram social-link">
          <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 169.063 169.063" enable-background="new 0 0 169.063 169.063">
            <path d="M122.406 0h-75.752c-25.725 0-46.654 20.93-46.654 46.655v75.752c0 25.726 20.929 46.655 46.654 46.655h75.752c25.727 0 46.656-20.93 46.656-46.655v-75.752c.001-25.725-20.929-46.655-46.656-46.655zm31.657 122.407c0 17.455-14.201 31.655-31.656 31.655h-75.753c-17.454.001-31.654-14.2-31.654-31.655v-75.752c0-17.454 14.2-31.655 31.654-31.655h75.752c17.455 0 31.656 14.201 31.656 31.655v75.752zm-69.532-81.437c-24.021 0-43.563 19.542-43.563 43.563 0 24.02 19.542 43.561 43.563 43.561s43.563-19.541 43.563-43.561c0-24.021-19.542-43.563-43.563-43.563zm0 72.123c-15.749 0-28.563-12.812-28.563-28.561 0-15.75 12.813-28.563 28.563-28.563s28.563 12.813 28.563 28.563c0 15.749-12.814 28.561-28.563 28.561zm45.39-84.842c-2.89 0-5.729 1.17-7.77 3.22-2.051 2.04-3.23 4.88-3.23 7.78 0 2.891 1.18 5.73 3.23 7.78 2.04 2.04 4.88 3.22 7.77 3.22 2.9 0 5.73-1.18 7.78-3.22 2.05-2.05 3.22-4.89 3.22-7.78 0-2.9-1.17-5.74-3.22-7.78-2.04-2.05-4.88-3.22-7.78-3.22z"></path>
          </svg>
          <span>Goodwill Detroit Instagram</span>
        </a>
        <a href="<?php the_field('twitter-url','options') ?>" target="_blank" class="twitter social-link">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612" width="512" height="512" enable-background="new 0 0 612 612">
            <path d="M612 116.258c-22.525 9.981-46.694 16.75-72.088 19.772 25.929-15.527 45.777-40.155 55.184-69.411-24.322 14.379-51.169 24.82-79.775 30.48-22.907-24.437-55.49-39.658-91.63-39.658-69.334 0-125.551 56.217-125.551 125.513 0 9.828 1.109 19.427 3.251 28.606-104.326-5.24-196.835-55.223-258.75-131.174-10.823 18.51-16.98 40.078-16.98 63.101 0 43.559 22.181 81.993 55.835 104.479-20.575-.688-39.926-6.348-56.867-15.756v1.568c0 60.806 43.291 111.554 100.693 123.104-10.517 2.83-21.607 4.398-33.08 4.398-8.107 0-15.947-.803-23.634-2.333 15.985 49.907 62.336 86.199 117.253 87.194-42.947 33.654-97.099 53.655-155.916 53.655-10.134 0-20.116-.612-29.944-1.721 55.567 35.681 121.536 56.485 192.438 56.485 230.948 0 357.188-191.291 357.188-357.188l-.421-16.253c24.666-17.593 46.005-39.697 62.794-64.861z"></path>
          </svg>
          <span>Goodwill Detroit Twitter</span>
        </a>
        <a href="<?php the_field('linkedin-url','options') ?>" target="_blank" class="linkedin social-link">
          <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.117 430.117" enable-background="new 0 0 430.117 430.117">
            <path d="M430.117 261.543v159.017h-92.188v-148.367c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477v154.873h-92.219s1.242-251.285 0-277.32h92.21v39.309l-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zm-377.934-251.985c-31.548 0-52.183 20.693-52.183 47.905 0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zm-46.706 411.002h92.184v-277.32h-92.184v277.32z"></path>
          </svg>
          <span>Goodwill Detroit Linkedin</span>
        </a>
      </div>
    </div>
    <div class="footer-contents">
      <div class="top max-width">
        <img class="footer-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/Goodwill-Logo.svg" alt="Goodwill Logo" />
        <nav>
          <?php wp_nav_menu( array( 'theme_location' => 'footer-nav' ) ); ?>
        </nav> 
      </div>
      <div class="bottom max-width flex-container">
        <div class="full-width">
          <div style="width: 100%; text-align: left; margin: 1rem 0 -1rem 0;"><img class="aligncenter size-full wp-image-1253" style="display: inline-block; margin: .5rem 1rem; vertical-align: middle;" src="https://e5goodwill.wpengine.com/wp-content/uploads/2017/04/bitmap.png" alt="" width="67" height="68" /><img class="aligncenter size-full wp-image-1252" style="display: inline-block; margin: .5rem 1rem; vertical-align: middle;" src="https://e5goodwill.wpengine.com/wp-content/uploads/2017/04/ajc-caps-rgb-2.png" alt="" width="228" height="32" /></div>
        </div>
        <div class="one-half">
          <?php the_field('footer_content', 'options') ?>
        </div>
        <div class="one-half">
          <h4>What Goodwill Industries of Greater Detroit has been up to</h4>
          <?php //2 LATEST POSTS
            $args = array( 'posts_per_page'  => 2, );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <div class="latest-news flex-container">
              <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                <div class="post-preview one-half">
                  <h5><?php the_title(); ?></h5>
                  <p><?php echo get_excerpt(); ?></p>
                  <a href="<?php the_permalink(); ?>" class="btn primary-btn icon"><span>Read the rest</span></a>
                </div>
              <?php endwhile; ?>
            </div>
          <?php } ?>

          <?php wp_reset_postdata(); ?>
        </div>
      </div>
    </div>
    <div class="social-links max-width mobile">
      <a href="<?php the_field('facebook-url','options') ?>" target="_blank" class="facebook social-link">
        <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.113 430.114" enable-background="new 0 0 430.113 430.114">
          <path d="M158.081 83.3v59.218h-43.385v72.412h43.385v215.183h89.122v-215.177h59.805s5.601-34.721 8.316-72.685h-67.784v-49.511c0-7.4 9.717-17.354 19.321-17.354h48.557v-75.385h-66.021c-93.519-.005-91.316 72.479-91.316 83.299z"></path>
        </svg>
        <span>Goodwill Detroit Facebook</span>
      </a>
      <a href="<?php the_field('instagram-url','options') ?>" target="_blank" class="instagram social-link">
        <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 169.063 169.063" enable-background="new 0 0 169.063 169.063">
          <path d="M122.406 0h-75.752c-25.725 0-46.654 20.93-46.654 46.655v75.752c0 25.726 20.929 46.655 46.654 46.655h75.752c25.727 0 46.656-20.93 46.656-46.655v-75.752c.001-25.725-20.929-46.655-46.656-46.655zm31.657 122.407c0 17.455-14.201 31.655-31.656 31.655h-75.753c-17.454.001-31.654-14.2-31.654-31.655v-75.752c0-17.454 14.2-31.655 31.654-31.655h75.752c17.455 0 31.656 14.201 31.656 31.655v75.752zm-69.532-81.437c-24.021 0-43.563 19.542-43.563 43.563 0 24.02 19.542 43.561 43.563 43.561s43.563-19.541 43.563-43.561c0-24.021-19.542-43.563-43.563-43.563zm0 72.123c-15.749 0-28.563-12.812-28.563-28.561 0-15.75 12.813-28.563 28.563-28.563s28.563 12.813 28.563 28.563c0 15.749-12.814 28.561-28.563 28.561zm45.39-84.842c-2.89 0-5.729 1.17-7.77 3.22-2.051 2.04-3.23 4.88-3.23 7.78 0 2.891 1.18 5.73 3.23 7.78 2.04 2.04 4.88 3.22 7.77 3.22 2.9 0 5.73-1.18 7.78-3.22 2.05-2.05 3.22-4.89 3.22-7.78 0-2.9-1.17-5.74-3.22-7.78-2.04-2.05-4.88-3.22-7.78-3.22z"></path>
        </svg>
        <span>Goodwill Detroit Instagram</span>
      </a>
      <a href="<?php the_field('twitter-url','options') ?>" target="_blank" class="twitter social-link">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612" width="512" height="512" enable-background="new 0 0 612 612">
          <path d="M612 116.258c-22.525 9.981-46.694 16.75-72.088 19.772 25.929-15.527 45.777-40.155 55.184-69.411-24.322 14.379-51.169 24.82-79.775 30.48-22.907-24.437-55.49-39.658-91.63-39.658-69.334 0-125.551 56.217-125.551 125.513 0 9.828 1.109 19.427 3.251 28.606-104.326-5.24-196.835-55.223-258.75-131.174-10.823 18.51-16.98 40.078-16.98 63.101 0 43.559 22.181 81.993 55.835 104.479-20.575-.688-39.926-6.348-56.867-15.756v1.568c0 60.806 43.291 111.554 100.693 123.104-10.517 2.83-21.607 4.398-33.08 4.398-8.107 0-15.947-.803-23.634-2.333 15.985 49.907 62.336 86.199 117.253 87.194-42.947 33.654-97.099 53.655-155.916 53.655-10.134 0-20.116-.612-29.944-1.721 55.567 35.681 121.536 56.485 192.438 56.485 230.948 0 357.188-191.291 357.188-357.188l-.421-16.253c24.666-17.593 46.005-39.697 62.794-64.861z"></path>
        </svg>
        <span>Goodwill Detroit Twitter</span>
      </a>
      <a href="<?php the_field('linkedin-url','options') ?>" target="_blank" class="linkedin social-link">
        <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.117 430.117" enable-background="new 0 0 430.117 430.117">
          <path d="M430.117 261.543v159.017h-92.188v-148.367c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477v154.873h-92.219s1.242-251.285 0-277.32h92.21v39.309l-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zm-377.934-251.985c-31.548 0-52.183 20.693-52.183 47.905 0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zm-46.706 411.002h92.184v-277.32h-92.184v277.32z"></path>
        </svg>
        <span>Goodwill Detroit Linkedin</span>
      </a>
    </div>
    <div class="copyright-container">
      <div class="max-width">
        <p id="copyright">©Copyright <?php echo date('Y'); ?> Goodwill Industries of Detroit. All Rights Reserved.</p> 
        <p id="e5-credit">Crafted by <a href="https://element5digital.com/">Element5</a></p>
      </div>
    </div>
  </footer>
<?php } ?>