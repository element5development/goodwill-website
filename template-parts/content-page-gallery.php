<?php /*
DISPLAY MISSION: PHOTO, QUOTE, TEXT, SLIDER GALLERY
*/ ?>

<section class="gallery-container flex-container max-width">
  <?php 
    $images = get_field('page_gallery');
    $i = 0; 
  ?>
  <?php foreach( $images as $image ): $i++;?>

    <div class="gallery-item <?php if( $i == 1 ) {?>grid-sizer<?php } ?> ">
      <div class="container">
        <a href="<?php echo $image['url']; ?>" class="fluidbox full-width"><img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" title="" /></a>
        <?php if( is_page(510) ) { ?><p><?php echo $image['description']; ?></p><?php } ?>
      </div>
    </div>

   <?php endforeach; ?>
</section>