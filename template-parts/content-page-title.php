<?php /*
DISPLAY PAGE TITLE, PAGE ICON, PAGE DESCRIPTION AND FEATURED IMAGE
*/ ?>

<?php //GET BACKGROUND IMAGE
if ( has_post_thumbnail() ) {
  $thumb_id = get_post_thumbnail_id();
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
  $thumb_url = $thumb_url_array[0];
} else if ( is_post_type_archive( 'jobs' ) ) {
  $thumb_url_array = get_field('default_jobs_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} else if ( is_home() || is_archive() ) {
  $thumb_url_array = get_field('featured_news_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} else if ( is_singular('jobs') ) {
  $thumb_url_array = get_field('default_single_jobs_copy', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} else if ( is_single() ) {
  $thumb_url_array = get_field('default_post_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} else {
  $thumb_url_array = get_field('default_page_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
}
?>

<?php 
$thiscat =  get_query_var('cat'); // The id of the current category
$catobject = get_category($thiscat,false); // Get the Category object by the id of current category
$parentcat = $catobject->category_parent; // the id of the parent category 
?>


<?php if ( is_front_page() ) { //SLIDER TOP FOR HOME PAGE ?>

  <?php if( have_rows('slides') ) { ?>
    <section class="page-title-slider slider-container full-width dark-bg">
      <?php while ( have_rows('slides') ) : the_row(); ?>

        <div class="single-slide">
          <div class="slide-content one-half full vertical-align-parent dark-bg">
            <div class="vertical-align-child">
              <h1><?php the_sub_field('slide_title'); ?></h1>
              <?php if ( get_sub_field('slide_description') ) { ?>
                <p class="slide-subheading"><?php the_sub_field('slide_description'); ?></p>
              <?php } ?>
              <?php if ( get_sub_field('cta_url') ) { ?>
                <a href="<?php the_sub_field('cta_url'); ?>" class="btn primary-btn icon"><span><?php the_sub_field('cta_text'); ?></span></a>
              <?php } ?>
            </div>
          </div>
          <?php $image = get_sub_field('slide_image'); ?>
          <div class="slide-image one-half full" style="background-image: url('<?php echo $image['url']; ?>');"></div>
        </div>

      <?php endwhile; ?>
    </section>
  <?php } else { 
      // NO SLIDES
  } ?>
<?php } elseif ( is_post_type_archive( 'jobs' ) || $parentcat == 16 || $parentcat == 12 ) { //JOBS ?>

  <section class="page-title-container full-width vertical-align-parent dark-bg" style="background-image: url('<?php echo $thumb_url ?>');">
    <div class="page-title-contents vertical-align-child">
        <div class="page-icon" style="background-image: url('/wp-content/uploads/2016/10/icon-briefcase-1.svg');"></div>
        <h1 class="page-title">Be part of something greater.</h1>
    </div>
    <div class="page-title-overlay"></div>
  </section>
  <?php if ( function_exists('yoast_breadcrumb') ) { ?>
    <div class="breadcrumbs-nav">
      <div class="max-width">
        <?php yoast_breadcrumb('<p>','</p>'); ?>
      </div>
    </div>
  <?php } ?>

<?php } elseif ( is_home() || is_archive() ) { //PRESS || ARCHIVE ?>

  <section class="page-title-container full-width vertical-align-parent dark-bg" style="background-image: url('<?php echo $thumb_url ?>');">
    <div class="page-title-contents vertical-align-child">
        <div class="page-icon" style="background-image: url('/wp-content/uploads/2016/10/icon-press.svg');"></div>
        <h1 class="page-title">In the Press</h1>
    </div>
    <div class="page-title-overlay"></div>
  </section>
  <?php if ( function_exists('yoast_breadcrumb') ) { ?>
    <div class="breadcrumbs-nav">
      <div class="max-width">
        <?php yoast_breadcrumb('<p>','</p>'); ?>
      </div>
    </div>
  <?php } ?>

<?php } elseif ( is_search() ) { //SEARCH TERMS ?>

  <section class="page-title-container full-width vertical-align-parent dark-bg" style="background-image: url('<?php echo $thumb_url ?>');">
    <div class="page-title-contents vertical-align-child">
      <?php $term = (isset($_GET['s'])) ? $_GET['s'] : ''; // Get 's' querystring param ?>
        <h1 class="page-title">Search for "<?php echo $term; ?>"</h1>
    </div>
    <div class="page-title-overlay"></div>
  </section>
  <?php if ( function_exists('yoast_breadcrumb') ) { ?>
    <div class="breadcrumbs-nav">
      <div class="max-width clearfix">
        <?php yoast_breadcrumb('<p>','</p>'); ?>
        <?php if( is_single() ) { ?><p class="post-date">Published: <?php the_date('m/d/y'); ?></p><?php } ?>
      </div>
    </div>
  <?php } ?>

<?php } elseif ( is_page(392) || is_singular( 'hero-stories' ) ) { //HEROS AND HERO STORIES ?>

<?php if( have_rows('slides','options') ) { ?>
  <section class="page-title-container full-width vertical-align-parent dark-bg">
      <div class="page-title-contents vertical-align-child">
      <!-- <div class="page-icon" style="background-image: url('/wp-content/uploads/2016/10/icon-heart-hands.svg');"></div> -->
      <h1 class="page-title">Information that’s Good to Know</h1>
      <!-- <p class="page-description">A place where we can reflect together about what we do and why it matters.</p> -->
      <?php if ( get_field('page_cta_url') ) { ?>
        <a href="<?php the_field('page_cta_url') ?>" class="btn primary-btn icon"><span><?php the_field('page_cta') ?></span></a>
      <?php } ?>
    </div>
    <div class="page-title-overlay"></div>
    <div class="slider-container heros-slider">
      <?php while ( have_rows('slides','options') ) : the_row(); ?>

        <div class="single-slide">
          <?php $image = get_sub_field('slide_image','options'); ?>
          <div class="slide-image full-width" style="background-image: url('<?php echo $image['url']; ?>');"></div>
        </div>

      <?php endwhile; ?>
    </div>
  </section>
  <?php if ( function_exists('yoast_breadcrumb') ) { ?>
    <div class="breadcrumbs-nav">
      <div class="max-width">
        <?php yoast_breadcrumb('<p>','</p>'); ?>
        <a href="/reflections/" class="btn primary-btn reflections-link"><span>View All</span></a>
      </div>
    </div>
  <?php } ?>
<?php } else { 
    // NO SLIDES
} ?>

<?php } elseif ( is_page(505) ) { //SUCCESS STORIES ?>

  <?php if( have_rows('slides') ) { ?>
    <section class="page-title-container full-width vertical-align-parent dark-bg">
      <div class="page-title-contents vertical-align-child">
        <?php if ( get_field('page_icon') ) { ?>
          <?php $image = get_field('page_icon'); ?>
          <div class="page-icon" style="background-image: url('<?php echo $image['url']; ?>');"></div>
        <?php } ?>
        <?php if ( get_field('page_heading') ) { ?>
          <h1 class="page-title"><?php the_field('page_heading'); ?></h1>
        <?php } else { ?>
          <h1 class="page-title"><?php the_title(); ?></h1>
        <?php } ?>
        <?php if ( get_field('page_description') ) { ?>
          <p class="page-description"><?php the_field('page_description'); ?></p>
        <?php } ?>
        <?php if ( get_field('page_cta_url') ) { ?>
          <a href="<?php the_field('page_cta_url') ?>" class="btn primary-btn icon"><span><?php the_field('page_cta') ?></span></a>
        <?php } ?>
      </div>
      <div class="page-title-overlay"></div>
      <div class="slider-container heros-slider">
        <?php while ( have_rows('slides') ) : the_row(); ?>

          <div class="single-slide">
            <?php $image = get_sub_field('slide_image'); ?>
            <div class="slide-image full-width" style="background-image: url('<?php echo $image['url']; ?>');"></div>
          </div>

        <?php endwhile; ?>
      </div>
    </section>
    <?php if ( function_exists('yoast_breadcrumb') ) { ?>
      <div class="breadcrumbs-nav">
        <div class="max-width">
          <?php yoast_breadcrumb('<p>','</p>'); ?>
        </div>
      </div>
    <?php } ?>
  <?php } else { 
      // NO SLIDES
  } ?>

<?php } else { ?>

  <section class="page-title-container full-width vertical-align-parent dark-bg" style="background-image: url('<?php echo $thumb_url ?>');">
    <div class="page-title-contents vertical-align-child">
      <?php if ( get_field('page_icon') ) { ?>
        <?php $image = get_field('page_icon'); ?>
        <div class="page-icon" style="background-image: url('<?php echo $image['url']; ?>');"></div>
      <?php } ?>
      <?php if ( get_field('page_heading') ) { ?>
        <h1 class="page-title"><?php the_field('page_heading'); ?></h1>
      <?php } else { ?>
        <h1 class="page-title"><?php the_title(); ?></h1>
      <?php } ?>
      <?php if ( get_field('page_description') ) { ?>
        <p class="page-description"><?php the_field('page_description'); ?></p>
      <?php } ?>
      <?php if ( get_field('page_cta_url') ) { ?>
        <a href="<?php the_field('page_cta_url') ?>" class="btn primary-btn icon"><span><?php the_field('page_cta') ?></span></a>
      <?php } ?>
    </div>
    <div class="page-title-overlay"></div>
  </section>
  <?php if ( function_exists('yoast_breadcrumb') ) { ?>
    <div class="breadcrumbs-nav">
      <div class="max-width clearfix">
        <?php yoast_breadcrumb('<p>','</p>'); ?>
        <?php if( is_single() && !is_singular('jobs') ) { ?><p class="post-date">Published: <?php the_date('m/d/y'); ?></p><?php } ?>
      </div>
    </div>
  <?php } ?>

<?php } ?>