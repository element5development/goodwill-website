<?php /*
MAIN NAVIGATION
*/ ?>

<?php if (  is_page_template( 'template-styleguide.php' ) ) { ?>
  <header id="sticky-nav" class="primary-nav full-width dark-bg">
    <div class="max-width clearfix">
      <a href="/"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/Goodwill-Logo.svg" alt="Goodwill Logo" style="width: 50px; margin: 10px 0;"/></a>
      <h2>Styleguide</h2>
    </div>
  </header>
<?php } else { ?>
  <header id="sticky-nav" class="primary-nav full-width">
    <div id="search-open" class="search-container dark-bg">
      <div class="max-width">
        <?php get_search_form() ?>
        <div id="search-close" class="search-close">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50">
            <path class="cls-1" d="M42.6 0h-35.2a7.58 7.58 0 0 0-7.4 7.65v35a7.32 7.32 0 0 0 7.4 7.35h35.2a7.32 7.32 0 0 0 7.4-7.34v-35a7.58 7.58 0 0 0-7.4-7.66zm-5.54 36.36l-1.33 1.33-11-11-11 11-1.33-1.33 11-11-11-11 1.33-1.33 11 11 11-11 1.33 1.33-11 11z"></path>
          </svg>
        </div>
      </div>
    </div>
    <div class="max-width flex-container clearfix">
      <div class="utility-nav flex-container">
        <div class="one-third flex-container">
          <a href="tel:<?php the_field('phone', 'options'); ?>" class="one-third" title="Call Goodwill Detroit">
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="33" viewBox="0 0 32 33">
              <path d="M24.74 21.34c-.64-.42-2.22-.86-3.46.68-1.26 1.54-2.8 4.12-8.64-1.72-5.86-5.84-3.28-7.4-1.74-8.64 1.54-1.26 1.1-2.84.68-3.48-.42-.64-3.14-4.84-3.62-5.54-.48-.7-1.12-1.82-2.62-1.62-1.1.16-5.34 2.42-5.34 7.2s3.76 10.64 8.92 15.8c5.14 5.14 11.02 8.9 15.8 8.9 4.78 0 7.02-4.22 7.18-5.34.22-1.48-.92-2.14-1.62-2.62-.7-.48-4.88-3.2-5.54-3.62"></path>
            </svg>
            <span>Call Goodwill Detroit</span>
          </a>
          <a href="<?php the_field('directions_url', 'options'); ?>" target="_blank" class="one-third" title="Directions to Goodwill Detroit">
            <svg xmlns="http://www.w3.org/2000/svg" width="38" height="51" viewBox="0 0 38 51">
              <path d="M12.54 18.81c0-3.45 2.82-6.27 6.27-6.27 3.48 0 6.3 2.82 6.3 6.27 0 3.48-2.82 6.3-6.3 6.3-3.45 0-6.27-2.82-6.27-6.3zm-6.27 0c0 6.96 5.61 12.57 12.54 12.57 6.96 0 12.57-5.61 12.57-12.57 0-6.93-5.61-12.54-12.57-12.54-6.93 0-12.54 5.61-12.54 12.54zm-6.27 0c0-10.38 8.43-18.81 18.81-18.81 10.41 0 18.84 8.43 18.84 18.81 0 10.41-18.84 31.38-18.84 31.38s-18.81-20.97-18.81-31.38z"></path>
            </svg>
            <span>Directions to Goodwill Detroit</span>
          </a>
          <a href="#"" class="tool-tip-container one-third" title="Improve the Goodwill Detroit website">
            <svg xmlns="http://www.w3.org/2000/svg" width="31" height="31" viewBox="0 0 31 31">
              <path d="M17.42 24.18h-3.88v-1.92h.98v-8.7h-.98v-1.94h2.9v10.64h.98zm-.98-16.9c.32.3.5.74.5 1.18 0 .46-.18.88-.5 1.2-.32.32-.76.5-1.2.5-.44 0-.88-.18-1.2-.5-.32-.3-.5-.74-.5-1.2 0-.44.18-.88.5-1.18.64-.64 1.76-.64 2.4 0zm-16.44 8.2c0 8.54 6.94 15.48 15.48 15.48s15.48-6.94 15.48-15.48-6.94-15.48-15.48-15.48-15.48 6.94-15.48 15.48z"></path>
            </svg>
            <span class="tool-tip">
              We take steps so all people can use this site. Here is what we did to make the site better for all.<br/>
              * A "skip nav" link for people who can’t see well<br/>
              * All text can be made larger or smaller<br/>
              * All colors have high contrast for older people<br/>
              * You can browse the whole site with your tab key<br/>
              * All pictures have text for people who can’t see well<br/><br/>
              How can we make this site better for you? Let us know! We are always making the site better.
            </span>
          </a>
        </div>
        <div class="one-third social-links flex-container" title="Goodwill Detroit Facebook">
          <a href="<?php the_field('facebook-url', 'options'); ?>" target="_blank" class="facebook social-link">
            <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.113 430.114" enable-background="new 0 0 430.113 430.114">
              <path d="M158.081 83.3v59.218h-43.385v72.412h43.385v215.183h89.122v-215.177h59.805s5.601-34.721 8.316-72.685h-67.784v-49.511c0-7.4 9.717-17.354 19.321-17.354h48.557v-75.385h-66.021c-93.519-.005-91.316 72.479-91.316 83.299z"></path>
            </svg>
            <span>Goodwill Detroit Facebook</span>
          </a>
          <a href="<?php the_field('instagram-url', 'options'); ?>" target="_blank" class="instagram social-link" title="Goodwill Detroit Instagram">
            <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 169.063 169.063" enable-background="new 0 0 169.063 169.063">
              <path d="M122.406 0h-75.752c-25.725 0-46.654 20.93-46.654 46.655v75.752c0 25.726 20.929 46.655 46.654 46.655h75.752c25.727 0 46.656-20.93 46.656-46.655v-75.752c.001-25.725-20.929-46.655-46.656-46.655zm31.657 122.407c0 17.455-14.201 31.655-31.656 31.655h-75.753c-17.454.001-31.654-14.2-31.654-31.655v-75.752c0-17.454 14.2-31.655 31.654-31.655h75.752c17.455 0 31.656 14.201 31.656 31.655v75.752zm-69.532-81.437c-24.021 0-43.563 19.542-43.563 43.563 0 24.02 19.542 43.561 43.563 43.561s43.563-19.541 43.563-43.561c0-24.021-19.542-43.563-43.563-43.563zm0 72.123c-15.749 0-28.563-12.812-28.563-28.561 0-15.75 12.813-28.563 28.563-28.563s28.563 12.813 28.563 28.563c0 15.749-12.814 28.561-28.563 28.561zm45.39-84.842c-2.89 0-5.729 1.17-7.77 3.22-2.051 2.04-3.23 4.88-3.23 7.78 0 2.891 1.18 5.73 3.23 7.78 2.04 2.04 4.88 3.22 7.77 3.22 2.9 0 5.73-1.18 7.78-3.22 2.05-2.05 3.22-4.89 3.22-7.78 0-2.9-1.17-5.74-3.22-7.78-2.04-2.05-4.88-3.22-7.78-3.22z"></path>
            </svg>
            <span>Goodwill Detroit Instagram</span>
          </a>
          <a href="<?php the_field('twitter-url', 'options'); ?>" target="_blank" class="twitter social-link" title="Goodwill Detroit Twitter">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612" width="512" height="512" enable-background="new 0 0 612 612">
              <path d="M612 116.258c-22.525 9.981-46.694 16.75-72.088 19.772 25.929-15.527 45.777-40.155 55.184-69.411-24.322 14.379-51.169 24.82-79.775 30.48-22.907-24.437-55.49-39.658-91.63-39.658-69.334 0-125.551 56.217-125.551 125.513 0 9.828 1.109 19.427 3.251 28.606-104.326-5.24-196.835-55.223-258.75-131.174-10.823 18.51-16.98 40.078-16.98 63.101 0 43.559 22.181 81.993 55.835 104.479-20.575-.688-39.926-6.348-56.867-15.756v1.568c0 60.806 43.291 111.554 100.693 123.104-10.517 2.83-21.607 4.398-33.08 4.398-8.107 0-15.947-.803-23.634-2.333 15.985 49.907 62.336 86.199 117.253 87.194-42.947 33.654-97.099 53.655-155.916 53.655-10.134 0-20.116-.612-29.944-1.721 55.567 35.681 121.536 56.485 192.438 56.485 230.948 0 357.188-191.291 357.188-357.188l-.421-16.253c24.666-17.593 46.005-39.697 62.794-64.861z"></path>
            </svg>
            <span>Goodwill Detroit Twitter</span>
          </a>
          <a href="<?php the_field('linkedin-url', 'options'); ?>" target="_blank" class="linkedin social-link" title="Goodwill Detroit Linkedin">
            <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.117 430.117" enable-background="new 0 0 430.117 430.117">
              <path d="M430.117 261.543v159.017h-92.188v-148.367c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477v154.873h-92.219s1.242-251.285 0-277.32h92.21v39.309l-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zm-377.934-251.985c-31.548 0-52.183 20.693-52.183 47.905 0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zm-46.706 411.002h92.184v-277.32h-92.184v277.32z"></path>
            </svg>
            <span>Goodwill Detroit Linkedin</span>
          </a>
        </div>
        <div class="one-third flex-container">
          <a href="#main-anchor" class="skip one-third smoothScroll" title="Skip Nav">
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
              <path d="M16.32 24.68v.02l-.7.8-6.18-7.28c-.34-.42-.3-1.04.12-1.38.42-.36 1.02-.3 1.38.1l3.72 4.4v-14.5c0-.54.42-.98.96-.98s.98.44.98.98v14.5l3.7-4.38c.36-.4.98-.46 1.38-.12.42.36.46.98.12 1.38zm-16.32-9.06c0 8.64 7 15.64 15.62 15.64 8.64 0 15.64-7 15.64-15.64 0-8.62-7-15.62-15.64-15.62-8.62 0-15.62 7-15.62 15.62z"></path>
            </svg>
            <span>Skip Nav</span>
          </a>
          <a href="" id="font-increase" class="one-third" title="Increase Font Size">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 31.45">
              <path d="M47.54 37.52a5 5 0 0 1-1.42-.52 4.66 4.66 0 0 1-1.25-1 5.08 5.08 0 0 1-.72-1.19q-1.71-4-4.13-10.14t-6.71-16.67h-4.37q-3.08 7.43-5.63 13.59l-5.08 12.31a6.8 6.8 0 0 1-1 1.81 5.54 5.54 0 0 1-1.56 1.29 4.34 4.34 0 0 1-1.43.51 12.06 12.06 0 0 1-1.63.23v1.7h12.72v-1.7a11.48 11.48 0 0 1-3.53-.74c-.71-.33-1.06-.72-1.06-1.19a6.19 6.19 0 0 1 .08-.77 10.72 10.72 0 0 1 .49-1.77q.33-.94.78-2.14c.23-.61.44-1.15.64-1.65h11.07l2.46 6a2.62 2.62 0 0 1 .18.59 2.54 2.54 0 0 1 0 .39c0 .34-.53.61-1.6.84a17 17 0 0 1-2.43.43v1.7h16.59v-1.7a10 10 0 0 1-1.46-.21zm-15.1-11.31h-8.38l4.16-10.54zm-5.71 11.79a3.66 3.66 0 0 1-1-.39 3.49 3.49 0 0 1-.92-.75 3.76 3.76 0 0 1-.53-.88q-1.26-3-3-7.47t-5.03-12.23h-3.25l-4.15 10-3.71 9.08a5.12 5.12 0 0 1-.76 1.34 4 4 0 0 1-1.12.95 3.23 3.23 0 0 1-1.06.35 9 9 0 0 1-1.2.17v1.25h9.37v-1.23a8.51 8.51 0 0 1-2.6-.52c-.52-.24-.78-.53-.78-.88a4.78 4.78 0 0 1 .01-.56 7.75 7.75 0 0 1 .36-1.3q.24-.69.57-1.58l.85-2.17h7.41l2.17 5.36a2 2 0 0 1 .14.43 1.92 1.92 0 0 1 0 .29c0 .24-.39.45-1.18.62a15.85 15.85 0 0 1-2 .31v1.25h12.16v-1.25zm-17.24-8.48l3-7.59 3.08 7.59z" transform="translate(-1 -8)"></path>
            </svg>
            <span>Adjust Font Size</span>
          </a>
          <a href="#search-open" id="open-search" class="one-third" title="Search the Goodwill Detroit website">
            <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29" viewBox="0 0 29 29">
              <path d="M2.7 10.82c0 4.48 3.64 8.12 8.12 8.12 4.48 0 8.12-3.64 8.12-8.12 0-4.48-3.64-8.12-8.12-8.12-4.48 0-8.12 3.64-8.12 8.12zm25.36 13.42c.48.48.8 1.16.8 1.9 0 1.5-1.22 2.72-2.72 2.72-.74 0-1.42-.32-1.9-.8l-7.94-7.92c-1.6.94-3.48 1.5-5.48 1.5-5.98 0-10.82-4.84-10.82-10.82s4.84-10.82 10.82-10.82 10.82 4.84 10.82 10.82c0 2-.56 3.88-1.5 5.48z"></path>
            </svg>
            <span>Search Goodwill Detroit</span>
          </a>
        </div>
      </div>
      <a href="/" class="home-link">
        <div class="logo-container vertical-align-parent">
          <div class="vertical-align-child">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/Goodwill-Detroit.svg" alt="Goodwill Detroit" />
            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/Goodwill-Logo.svg" alt="Goodwill Logo" />
           </div>
        </div>
      </a>
      <div id="mobile-menu" class="mobile-menu">
        <span>Menu</span>
        <div class="menu-icon">
          <span class="top bar"></span>
          <span class="mid bar"></span>
          <span class="bot bar"></span>
        </div>
      </div>
      <nav>
        <?php wp_nav_menu( array( 'theme_location' => 'primary-nav' ) ); ?>
        <div class="social-links mobile-social">
          <a href="<?php the_field('facebook-url','options') ?>" target="_blank" class="facebook social-link">
            <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.113 430.114" enable-background="new 0 0 430.113 430.114">
              <path d="M158.081 83.3v59.218h-43.385v72.412h43.385v215.183h89.122v-215.177h59.805s5.601-34.721 8.316-72.685h-67.784v-49.511c0-7.4 9.717-17.354 19.321-17.354h48.557v-75.385h-66.021c-93.519-.005-91.316 72.479-91.316 83.299z"></path>
            </svg>
            <span>Goodwill Detroit Facebook</span>
          </a>
          <a href="<?php the_field('instagram-url','options') ?>" target="_blank" class="instagram social-link">
            <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 169.063 169.063" enable-background="new 0 0 169.063 169.063">
              <path d="M122.406 0h-75.752c-25.725 0-46.654 20.93-46.654 46.655v75.752c0 25.726 20.929 46.655 46.654 46.655h75.752c25.727 0 46.656-20.93 46.656-46.655v-75.752c.001-25.725-20.929-46.655-46.656-46.655zm31.657 122.407c0 17.455-14.201 31.655-31.656 31.655h-75.753c-17.454.001-31.654-14.2-31.654-31.655v-75.752c0-17.454 14.2-31.655 31.654-31.655h75.752c17.455 0 31.656 14.201 31.656 31.655v75.752zm-69.532-81.437c-24.021 0-43.563 19.542-43.563 43.563 0 24.02 19.542 43.561 43.563 43.561s43.563-19.541 43.563-43.561c0-24.021-19.542-43.563-43.563-43.563zm0 72.123c-15.749 0-28.563-12.812-28.563-28.561 0-15.75 12.813-28.563 28.563-28.563s28.563 12.813 28.563 28.563c0 15.749-12.814 28.561-28.563 28.561zm45.39-84.842c-2.89 0-5.729 1.17-7.77 3.22-2.051 2.04-3.23 4.88-3.23 7.78 0 2.891 1.18 5.73 3.23 7.78 2.04 2.04 4.88 3.22 7.77 3.22 2.9 0 5.73-1.18 7.78-3.22 2.05-2.05 3.22-4.89 3.22-7.78 0-2.9-1.17-5.74-3.22-7.78-2.04-2.05-4.88-3.22-7.78-3.22z"></path>
            </svg>
            <span>Goodwill Detroit Instagram</span>
          </a>
          <a href="<?php the_field('twitter-url','options') ?>" target="_blank" class="twitter social-link">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612" width="512" height="512" enable-background="new 0 0 612 612">
              <path d="M612 116.258c-22.525 9.981-46.694 16.75-72.088 19.772 25.929-15.527 45.777-40.155 55.184-69.411-24.322 14.379-51.169 24.82-79.775 30.48-22.907-24.437-55.49-39.658-91.63-39.658-69.334 0-125.551 56.217-125.551 125.513 0 9.828 1.109 19.427 3.251 28.606-104.326-5.24-196.835-55.223-258.75-131.174-10.823 18.51-16.98 40.078-16.98 63.101 0 43.559 22.181 81.993 55.835 104.479-20.575-.688-39.926-6.348-56.867-15.756v1.568c0 60.806 43.291 111.554 100.693 123.104-10.517 2.83-21.607 4.398-33.08 4.398-8.107 0-15.947-.803-23.634-2.333 15.985 49.907 62.336 86.199 117.253 87.194-42.947 33.654-97.099 53.655-155.916 53.655-10.134 0-20.116-.612-29.944-1.721 55.567 35.681 121.536 56.485 192.438 56.485 230.948 0 357.188-191.291 357.188-357.188l-.421-16.253c24.666-17.593 46.005-39.697 62.794-64.861z"></path>
            </svg>
            <span>Goodwill Detroit Instagram</span>
          </a>
          <a href="<?php the_field('linkedin-url','options') ?>" target="_blank" class="linkedin social-link">
            <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.117 430.117" enable-background="new 0 0 430.117 430.117">
              <path d="M430.117 261.543v159.017h-92.188v-148.367c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477v154.873h-92.219s1.242-251.285 0-277.32h92.21v39.309l-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zm-377.934-251.985c-31.548 0-52.183 20.693-52.183 47.905 0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zm-46.706 411.002h92.184v-277.32h-92.184v277.32z"></path>
            </svg>
            <span>Goodwill Detroit Instagram</span>
          </a>
        </div>
      </nav> 
    </div>
  </header>
<?php } ?>