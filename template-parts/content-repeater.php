<?php //GET BACKGROUND IMAGE
if ( has_post_thumbnail() ) {
  $thumb_id = get_post_thumbnail_id();
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
  $thumb_url = $thumb_url_array[0];
} else {
  $thumb_url_array = get_field('default_post_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
}
?>

<?php if ( 'hero-stories' == get_post_type() ) { ?>

   <a href="<?php the_permalink(); ?>"  class="hero-preview card-post post-preview grey-bg one-fourth">
    <?php if ( get_field('reflection_vs_story') == 'story' ) {
      $image = get_field('headshot'); ?>
				<div class="post-top" style="background-image: url('<?php echo $image['sizes']['medium']; ?>');"></div><?php
    } else { 
			if (get_field('featured_image')) {
				$image = get_field('featured_image'); 
				$image = $image['sizes']['medium'];
			} else {
				$thumb_url_array = get_field('default_post_image', 'options'); 
				$image = $thumb_url_array['url'];
			}
		?>
      <div class="post-top" style="background-image: url('<?php echo $image; ?>');"></div>
    <?php } ?>
    <div class="post-bottom">
      <h3><?php the_title(); ?></h3>
      <blockquote><p>
        <?php if ( get_field('reflection_vs_story') == 'story' ) {
          the_field('quote'); 
        } else {
           the_field('reflection_preview');  
        } ?>
      </p></blockquote>
      <div class="learn-more">Read More
        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
          <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
        </svg>
      </div>
    </div>
  </a>

<?php } elseif ( 'success-stories' == get_post_type() ) { ?>
  <div class="success-preview post-preview full-width">
    <div class="max-width flex-container">
      <div class="left-contents">
        <?php $image = get_field('headshot'); ?>
        <img class="headshot" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php if ( get_field('video_shortcode') ) { ?>
          <a class="video-link" href="<?php the_field('video_shortcode') ?>" data-lity>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1021.45 718.42" class="youtube">
              <defs>
                <style>.cls-1{fill:#fff;}.cls-2{fill:#420000;fill-rule:evenodd;opacity:0.12;}.cls-3{fill:url(#linear-gradient);}</style>
                <linearGradient id="linear-gradient" x1="512.5" y1="1.3" x2="512.5" y2="719.72" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#e52d27"/><stop offset="1" stop-color="#bf171d"/></linearGradient>
              </defs>
              <path id="Triangle" class="cls-1" d="M405.23 491.7l276-143-276-144z"></path>
              <path id="The_Sharpness" class="cls-2" d="M405.23 204.7l242 161.63 34-17.63z"></path>
              <path class="cls-3" d="M1011.23 154.98s-10-70.39-40.58-101.38c-38.84-40.69-82.42-40.9-102.35-43.27-142.94-10.33-357.35-10.33-357.35-10.33h-.44s-214.41 0-357.35 10.33c-19.93 2.37-63.49 2.58-102.35 43.27-30.58 30.99-40.58 101.38-40.58 101.38s-10.23 82.66-10.23 165.32v77.49c0 82.66 10.23 165.32 10.23 165.32s10 70.39 40.58 101.39c38.86 40.68 89.9 39.4 112.63 43.66 81.72 7.84 347.29 10.26 347.29 10.26s214.63-.32 357.57-10.66c20-2.38 63.5-2.58 102.35-43.27 30.58-30.99 40.58-101.38 40.58-101.38s10.2-82.66 10.2-165.32v-77.49c.02-82.66-10.2-165.32-10.2-165.32zm-606 336.72v-287l276 144z" id="Lozenge"></path>
            </svg>
            <p>
              Watch<br/>my story!
              <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
              </svg>
            </p>
          </a>
        <?php } ?>
      </div>
      <div class="right-contents">
        <blockquote><p><?php the_field('quote'); ?></p></blockquote>
        <div class="description"><?php the_content(); ?></div>
      </div>
    </div>
  </div>
<?php } elseif ( 'jobs' == get_post_type() ) { ?>
  <a href="<?php the_permalink(); ?>" class="job-preview one-half">
    <div class="description flex-container">
      <h3><?php the_title(); ?></h3>
      <div class="text-description">
        <?php echo get_excerpt(); ?>
        <div class="learn-more">Read the Rest
          <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
            <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
          </svg>
        </div>
      </div>
    </div>
    <div class="details flex-container">
      <div class="location-type">
        <div class="location">
          <svg xmlns="http://www.w3.org/2000/svg" width="38" height="51" viewBox="0 0 38 51">
            <path d="M12.54 18.81c0-3.45 2.82-6.27 6.27-6.27 3.48 0 6.3 2.82 6.3 6.27 0 3.48-2.82 6.3-6.3 6.3-3.45 0-6.27-2.82-6.27-6.3zm-6.27 0c0 6.96 5.61 12.57 12.54 12.57 6.96 0 12.57-5.61 12.57-12.57 0-6.93-5.61-12.54-12.57-12.54-6.93 0-12.54 5.61-12.54 12.54zm-6.27 0c0-10.38 8.43-18.81 18.81-18.81 10.41 0 18.84 8.43 18.84 18.81 0 10.41-18.84 31.38-18.84 31.38s-18.81-20.97-18.81-31.38z"></path>
          </svg>
          <?php the_field('city_state') ?>
        </div>
        <div class="type">
          <svg xmlns="http://www.w3.org/2000/svg" width="43" height="43" viewBox="0 0 43 43">
            <path d="M30.36 18.48c-1.11 0-1.98.87-1.98 1.98v13.86c0 1.08.87 1.98 1.98 1.98h3.96c1.08 0 1.98-.9 1.98-1.98v-13.86c0-1.11-.9-1.98-1.98-1.98zm-11.22-7.92c-1.11 0-1.98.87-1.98 1.98v21.78c0 1.08.87 1.98 1.98 1.98h3.96c1.08 0 1.98-.9 1.98-1.98v-21.78c0-1.11-.9-1.98-1.98-1.98zm-11.22 17.82c-1.08 0-1.98.87-1.98 1.98v3.96c0 1.08.9 1.98 1.98 1.98h3.96c1.08 0 1.98-.9 1.98-1.98v-3.96c0-1.11-.9-1.98-1.98-1.98zm-3.96-24.42h34.32v34.32h-34.32zm36.3 38.28c1.08 0 1.98-.9 1.98-1.98v-38.28c0-1.08-.9-1.98-1.98-1.98h-38.28c-1.08 0-1.98.9-1.98 1.98v38.28c0 1.08.9 1.98 1.98 1.98z"></path>
          </svg>
          <?php if( in_category(31) ) {  ?>
            Entery-level,
          <?php } elseif ( in_category(30) ) { ?>
            Mid-level,
          <?php } elseif ( in_category(48) ) { ?>
            Manual,
          <?php } elseif ( in_category(46) ) { ?>
            Professionals,
          <?php } elseif ( in_category(47) ) { ?>
            Supervisor,
          <?php } else { ?> 
            Executive,
          <?php } ?>
          <?php the_field('term'); ?>
        </div>
      </div>
      <div class="btn primary-btn icon"><span>APPLY Now</span></div>
    </div>
  </div>
<?php } else { ?>
  <a href="<?php the_permalink(); ?>"  class="card-post post-preview one-third">
    <div class="post-top" style="background-image: url('<?php echo $thumb_url ?>');">
      <span class="post-category">
        <?php foreach((get_the_category()) as $category) { 
            echo '<span class="single-cat">' . $category->cat_name . '</span>'; 
        } ?>
      </span>
      <div class="dark-overlay"></div>
    </div>
    <div class="post-bottom">
      <h3><?php the_title(); ?></h3>
      <?php the_excerpt(); ?>
      <div class="learn-more">Read the Rest
        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
          <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
        </svg>
      </div>
    </div>
  </a>
<?php } ?>