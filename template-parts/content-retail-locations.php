<?php /*
DISPLAY RETAIL LOCATIONS AND HOURS
*/ ?>


<section class="retail-container grey-bg">
  <a id="retail-stores" class="page-anchor"></a>
  <div class="section-title-container">
    <?php if ( get_field('retail__section_title') ) { ?>
      <h2><?php the_field('retail__section_title') ?></h2>
    <?php } ?>
    <?php if ( get_field('retail__section_subheading') ) { ?>
      <p><?php the_field('retail__section_subheading') ?></p>
    <?php } ?>
  </div>
  <div class="flex-container max-width">
    <div class="hours">
      <div class="dark-bg">
        <div class="icon" style="background-image: url(http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-store.svg);"></div>
        <h3>Store Hours — All Locations</h3>
        <p><?php the_field('location_hours') ?></p>
      </div>
      <div class="dark-bg">
        <div class="icon" style="background-image: url(http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg);"></div>
        <h3>Donation Hours — All Locations</h3>
        <p><?php the_field('donate_hours') ?></p>
      </div>
    </div>
    <div class="locations">
      <?php if( have_rows('locations') ) {
        while ( have_rows('locations') ) : the_row(); ?>

          <div class="single-location">
            <p class="name"><?php the_sub_field('location_name') ?></p>
            <a href="http://maps.google.com/?q=<?php the_sub_field('location_address') ?>" target="_blank" class="map">
              <svg width="13" height="17" viewBox="0 0 13 17" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.341 8.438c-1.155 0-2.092-.937-2.092-2.092s.937-2.092 2.092-2.092 2.092.937 2.092 2.092-.937 2.092-2.092 2.092m0-6.275c-2.31 0-4.183 1.873-4.183 4.183 0 2.31 1.873 4.183 4.183 4.183 2.31 0 4.183-1.873 4.183-4.183 0-2.31-1.873-4.183-4.183-4.183m0 14.641s-6.275-6.993-6.275-10.458 2.81-6.275 6.275-6.275c3.465 0 6.275 2.81 6.275 6.275 0 3.465-6.275 10.458-6.275 10.458"></path>
              </svg>
              <span class="address"><?php the_sub_field('location_addresss') ?></span>
              <span class="directions">
                Directions
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="23" viewBox="0 0 18 23">
                  <path d="M0 22.4v-22.4l17.32 11.92z" fill="#a5cb5a"/>
                </svg>
              </span>
            </a>
            <a href="tel:+1 <?php the_sub_field('location_phone') ?>" class="phone">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19" enable-background="new 0 0 19 19">
                <path d="M14.379 11.833c-.4-.2-1.3-.5-2 .4s-1.6 2.3-4.9-1c-3.3-3.3-1.9-4.2-1-4.9.9-.7.6-1.6.4-2-.2-.4-1.8-2.7-2.1-3.1-.3-.4-.6-1-1.5-.9-.6.1-3 1.4-3 4.1s2.1 6.1 5.1 9 6.3 5.1 9 5.1 4-2.4 4.1-3c.1-.8-.5-1.2-.9-1.5-.5-.4-2.9-2-3.2-2.2"></path>
              </svg>
              <?php the_sub_field('location_phone') ?>
            </a>
          </div>

        <?php endwhile;
      } else {
        // no rows found
      } ?>
    </div>
  </div>
</section>