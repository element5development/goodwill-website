<?php /*
DISPLAY ROI OUTCOMES
*/ ?>


<section class="roi-container">
  <a id="roi" class="page-anchor"></a>
  <div class="section-title-container">
    <?php if ( get_field('roi_section_title') ) { ?>
      <h2><?php the_field('roi_section_title') ?></h2>
    <?php } ?>
    <?php if ( get_field('roi_section_subheading') ) { ?>
      <h6><?php the_field('roi_section_subheading') ?></h6>
    <?php } ?>
  </div>
  


    <div class="flex-container roi">


          <div class="one-fourth">
            <div class="graphic">
              <canvas id="people-served"></canvas>
              <script>
                jQuery(document).ready(function() {
                  /*CHARTS CENTER TEXT*/
                  Chart.pluginService.register({
                    beforeDraw: function(chart) {
                      var width = chart.chart.width,
                          height = chart.chart.height,
                          ctx = chart.chart.ctx;
                          type = chart.config.type;
                      ctx.restore();
                      var fontSize = (height / 75).toFixed(2);
                      ctx.font = fontSize + "em sans-serif";
                      ctx.textBaseline = "middle";
                      if (type == 'doughnut') {
                        var text = "<?php the_field('stat_overall'); ?>",
                        textX = Math.round((width - ctx.measureText(text).width) / 2),
                        textY = height / 2;
                      } else if (type == 'pie') {
                        var text = "<?php the_field('stat_four_percent'); ?>",
                        textX = Math.round((width - ctx.measureText(text).width) / 2),
                        textY = height / 2;
                      }
                      ctx.fillStyle = 'black';
                      ctx.fillText(text, textX, textY);
                      ctx.save();
                    }
                  });
                  /*CHARTS*/
                  var stat_one = '<?php the_field('stat_one_oakland'); ?>';
                  var stat_two = '<?php the_field('stat_one_wayne'); ?>';
                  var stat_three = '<?php the_field('stat_one_macomb'); ?>';
                  var ctx = document.getElementById("people-served").getContext('2d');
                  var myChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                      labels: ["Oakland", "Wayne", "Macomb"],
                      datasets: [{
                        backgroundColor: [
                          "#a5cb5a",
                          "#005c75",
                          "#8a8573",
                        ],
                        data: [ stat_one, stat_two, stat_three]
                      }]
                    },
                    options: {
                      responsive: true,
                      cutoutPercentage: 80,
                      legend: {
                        display: false
                     },
                    },
                  })
                }); 
              </script>
            </div>
            <h3><?php the_field('stat_one_title'); ?></h3>
            <div class="details"><?php the_field('stat_one_details'); ?></div>
          </div>

          <div class="one-fourth">
            <div class="graphic people">
              <div class="legend">
                <p class="legal-text">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.43 48" <?php if ( $i < $full) { ?> class="full" <?php } ?> >
                    <circle cx="8.21" cy="4.92" r="4.92"></circle>
                    <path d="M30.71 12.19h-10.11a3.16 3.16 0 0 0-3.16 3.16v11.41a3.16 3.16 0 0 0 3.16 3.16h.77v16.64a2.44 2.44 0 0 0 2.44 2.44h3.7a2.44 2.44 0 0 0 2.44-2.44v-16.64h.77a3.16 3.16 0 0 0 3.16-3.16v-11.41a3.16 3.16 0 0 0-3.17-3.16z" transform="translate(-17.44 -1)"></path>
                  </svg>
                  = 125 people
                </p>
              </div>
              <div class="flex-container">
                <?php 
                  $i = 0;
                  $full = get_field('stat_two_number');
                  do { $i++
                ?>
                  <div class="person-container">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.43 48" <?php if ( $i < $full) { ?> class="full" <?php } ?> >
                      <circle cx="8.21" cy="4.92" r="4.92"></circle>
                      <path d="M30.71 12.19h-10.11a3.16 3.16 0 0 0-3.16 3.16v11.41a3.16 3.16 0 0 0 3.16 3.16h.77v16.64a2.44 2.44 0 0 0 2.44 2.44h3.7a2.44 2.44 0 0 0 2.44-2.44v-16.64h.77a3.16 3.16 0 0 0 3.16-3.16v-11.41a3.16 3.16 0 0 0-3.17-3.16z" transform="translate(-17.44 -1)"></path>
                    </svg>
                  </div>
                <?php } while ( $i < 20 ); ?>
              </div>
            </div>
            <h3><?php the_field('stat_two_title'); ?></h3>
            <div class="details"><?php the_field('stat_two_details'); ?></div>
          </div>

          <div class="one-fourth">
            <div class="graphic">
              <canvas id="wages"></canvas>
              <script>
                jQuery(document).ready(function() {
                  /*CHARTS*/
                  var stat_one = '<?php the_field('stat_three_goodwill'); ?>';
                  var stat_two = '<?php the_field('stat_three_other'); ?>';
                  var ctx = document.getElementById("wages").getContext('2d');
                  var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                      labels: ["Goodwill", "Other Guy"],
                      datasets: [{
                        backgroundColor: [
                            '#a5cb5a',
                            '#005c75'
                        ],
                        borderWidth: 1,
                        data: [stat_one, stat_two, 0],
                      }]
                    },
                    options: {
                      responsive: true,
                      cutoutPercentage: 80,
                      legend: {
                        display: false
                      },
                      tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function(tooltipItems, data) { 
                                return '$' + tooltipItems.yLabel;
                            }
                        }
                      },
                    },
                  })
                });
              </script>
            </div>
            <h3><?php the_field('stat_three_title'); ?></h3>
            <div class="details"><?php the_field('stat_three_details'); ?></div>
          </div>

          <div class="one-fourth">
            <div class="graphic">
              <canvas id="retention"></canvas>
              <script>
                jQuery(document).ready(function() {
                  /*CHARTS*/
                  var stat_one = '<?php the_field('stat_four_percent'); ?>';
                  var stat_two = '<?php the_field('stat_four_percent_fill'); ?>';
                  var ctx = document.getElementById("retention").getContext('2d');
                  var myChart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                      labels: ["remain working after 90 days"],
                      datasets: [{
                        backgroundColor: [
                          '#a5cb5a',
                          '#005c75'
                        ],
                        data: [ stat_one,stat_two ]
                      }]
                    },
                    options: {
                      responsive: true,
                      cutoutPercentage: 80,
                      legend: {
                        display: false
                      },
                      tooltips : {
                        enabled: false      
                      },
                    },
                  })
                }); 
              </script>
            </div>
            <h3><?php the_field('stat_four_title'); ?></h3>
            <div class="details"><?php the_field('stat_four_details'); ?></div>
          </div>

    </div>
</section>