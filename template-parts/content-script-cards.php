<?php /*
DISPLAY FLIP THE SCRIPT CARD REPEATER
*/ ?>



<?php if( have_rows('script_cards') ) { ?>
  <div class="max-width flex-container">
    <?php while ( have_rows('script_cards') ) : the_row(); ?>

      <div class="card-script full-width">
        <?php $image = get_sub_field('card_image'); ?>
        <div class="card-img" style="background-image: url('<?php echo $image['url']; ?>');"></div>
        <h3><?php the_sub_field('card_title') ?></h3>
        <a href="javascript:void(0)" class="read-more">Discover</a>
        <div class="card-contents-continued">
          <?php the_sub_field('card_content_continued') ?>
        </div>
      </div>

    <?php endwhile; ?>
  </div>
<?php } else { 
    // NO SLIDES
} ?>