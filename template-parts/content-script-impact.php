<?php /*
DISPLAY FLIP THE SCRIPT CARD REPEATER
*/ ?>



<?php if( have_rows('script_impact') ) { $i == 0;?>
  <div class="max-width flex-container">
    <?php while ( have_rows('script_impact') ) : the_row(); $i++ ?>

      <div class="impact-stat flex-container">
        <div class="impact-number vertical-align-parent">
          <div class="vertical-align-child">
            <span class="counter-<?php echo $i; ?> counter" data-count="<?php the_sub_field('impact_number') ?>">0</span>
          </div>
        </div>
        <div class="impaact-content">
          <?php the_sub_field('impact_content') ?>
        </div>
      </div>

    <?php endwhile; ?>
  </div>
<?php } else { 
    // NO SLIDES
} ?>