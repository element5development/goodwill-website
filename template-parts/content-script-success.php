<?php /*
DISPLAY FLIP THE SCRIPT CARD REPEATER
*/ ?>



<?php if( have_rows('videos') ) { ?>
  <div class="max-width flex-container">
    <?php while ( have_rows('videos') ) : the_row(); ?>

      <div class="success-video one-half">
        <?php the_sub_field('embed_code') ?>
      </div>

    <?php endwhile; ?>
  </div>
<?php } else { 
    // NO SLIDES
} ?>

<?php if( have_rows('testimonies') ) { ?>
  <div class="slider-container testimonies full-width">
    <?php while ( have_rows('testimonies') ) : the_row(); ?>

      <div class="single-testimony">
        <blockquote>
          <p>
            <?php the_sub_field('testimony'); ?>
            <span><?php the_sub_field('name'); ?></span>
          </p>
        </blockquote>
      </div>

    <?php endwhile; ?>
  </div>
<?php } else { 
    // NO SLIDES
} ?>