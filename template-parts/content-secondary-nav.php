<?php /*
DISPLAY SECONDARY ANCHOR NAV
*/ ?>

<?php 
$thiscat =  get_query_var('cat'); // The id of the current category
$catobject = get_category($thiscat,false); // Get the Category object by the id of current category
$parentcat = $catobject->category_parent; // the id of the parent category 
?>

<?php if ( is_page(390) ) { //ABOUT ?>

  <nav class="secondary-nav max-width">
    <ul>
      <li class="menu-item"><a href="#mission" class="smoothScroll">MISSION</a></li>
      <li class="menu-item"><a href="#history" class="smoothScroll">HISTORY</a></li>
      <li class="menu-item"><a href="#greenworks" class="smoothScroll">GREEN WORKS</a></li>
      <li class="menu-item"><a href="#auto" class="smoothScroll">GOODWILL AUTOMOTIVE</a></li>
      <li class="menu-item"><a href="#roi" class="smoothScroll">OUTCOMES & ROI</a></li>
      <li class="menu-item"><a href="#staff" class="smoothScroll">LEADERSHIP</a></li>
      <li class="menu-item"><a href="#accountability" class="smoothScroll">ACCOUNTABILITY</a></li>
    </ul>
  </nav>

<?php } elseif ( is_page(508) || is_page(507) ) { //INDIVIDUALS AND BUSINESS ?>

  <nav class="secondary-nav max-width">
    <ul>
      <li class="menu-item"><a href="<?php echo get_permalink(507); ?>" class="smoothScroll">FOR INDIVIDUALS</a></li>
      <li class="menu-item"><a href="<?php echo get_permalink(508); ?>" class="smoothScroll">FOR BUSINESSES</a></li>
    </ul>
  </nav>

<?php } elseif ( is_page(511) ) { //FLIP THE SCRIPT ?>

  <nav class="secondary-nav max-width">
    <ul>
      <li class="menu-item"><a href="#editor-contents" class="smoothScroll">THE PROGRAM</a></li>
      <li class="menu-item"><a href="#impact" class="smoothScroll">IMPACT DATA</a></li>
      <li class="menu-item"><a href="#success" class="smoothScroll">OUR SUCCESS STORIES</a></li>
      <li class="menu-item"><a href="#page-contact" class="smoothScroll">GET IN TOUCH</a></li>
    </ul>
  </nav>

<?php } else if ( 507 == $post->post_parent ) { //INDIVIDUALS CHILD PAGES ?>

  <nav class="secondary-nav max-width">
    <ul>
      <li class="menu-item"><a href="#editor-contents" class="smoothScroll">THE PROGRAM</a></li>
      <li class="menu-item"><a href="#page-contact" class="smoothScroll">GET IN TOUCH</a></li>
    </ul>
  </nav>

<?php } elseif ( is_page(395) ) { //DONTE ?>

  <nav class="secondary-nav max-width">
    <ul>
      <li class="menu-item"><a href="#donate-to" class="smoothScroll">DONATIONS TO GOODWILL</a></li>
      <li class="menu-item"><a href="#retail-stores" class="smoothScroll">GOODWILL RETAIL STORES</a></li>
      <li class="menu-item"><a href="#donate-what" class="smoothScroll">WHAT CAN I DONATE?</a></li>
      <li class="menu-item"><a href="#other-ways" class="smoothScroll">OTHER WAYS TO SUPPORT GOODWILL</a></li>
    </ul>
  </nav>

<?php } elseif ( is_post_type_archive( 'jobs' ) || $parentcat == 16 || $parentcat == 12 ) { //JOB ARCHIVE ?>  

  <nav class="secondary-nav blog-nav max-width">
    <h3>Sort Jobs By:</h3>
    <ul tabindex="-1">
      <li class="menu-item">JOB TYPE
        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
          <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
        </svg>
        <ul>
          <?php wp_list_categories( array(
            'use_desc_for_title' => false,
            'title_li'            => __( '' ),
            'child_of'           => 12,
            'hide_empty' => false
          ) ); ?>
        </ul>
      </li>
      <li class="menu-item">Location 
        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
          <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
        </svg>
        <ul>
          <li><a href="/jobs/?location=Detroit, MI">Detroit, MI</a></li>
          <li><a href="/jobs/?location=Livonia, MI">Livonia, MI</a></li>
          <li><a href="/jobs/?location=Commerce Twp., MI">Commerce Twp., MI</a></li>
          <li><a href="/jobs/?location=Canton, MI">Canton, MI</a></li>
          <li><a href="/jobs/?location=Dearborn, MI">Dearborn, MI</a></li>
          <li><a href="/jobs/?location=Woodhaven, MI">Woodhaven, MI</a></li>
          <li><a href="/jobs/?location=Ypsilanti, MI">Ypsilanti, MI</a></li>
          <li><a href="/jobs/?location=Novi, MI">Novi, MI</a></li>
          <li><a href="/jobs/?location=Highland, MI">Highland, MI</a></li>
          <li><a href="/jobs/?location=Westland, MI">Westland, MI</a></li>
        </ul>
      </li>
      <li class="menu-item">Part-Time/Full-Time 
        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
          <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
        </svg>
        <ul>
          <li><a href="/jobs/?term=Part-time">Part-time</a></li>
          <li><a href="/jobs/?term=Full-time">Full-time</a></li>
        </ul>
      </li>
    </ul>
  </nav>

<?php } elseif ( is_home() || is_archive() ) { //PRESS ?>

  <nav class="secondary-nav blog-nav max-width">
    <h3>Sort Press By:</h3>
    <ul>
      <li class="menu-item">DATE POSTED 
        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
          <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
        </svg>
        <ul>
          <li><a href="/press/?order=DESC&orderby=date">Newest</a></li>
          <li><a href="/press/?order=ASC&orderby=date">Oldest</a></li>
        </ul>
      </li>
      <li class="menu-item">POPULARITY
        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
          <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
        </svg>
        <ul>
          <li><a href="/press/?order=DESC&orderby=comment_count">Most</a></li>
          <li><a href="/press/?order=ASC&orderby=comment_count">Least</a></li>
        </ul>
      </li>
      <li class="menu-item">CATEGORY
        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
          <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
        </svg>
        <ul>
          <?php wp_list_categories( array(
            'use_desc_for_title' => false,
            'title_li'            => __( '' ),
            'child_of'           => 7
          ) ); ?>
        </ul>
      </li>
    </ul>
  </nav>

<?php } ?>