<?php /*
DISPLAY SINGLE SERVICE CONTACT FORM AND MAP
*/ ?>

<div class="page-contact-container">
  <a id="page-contact" class="page-anchor"></a>
  <div class="flex-container max-width">
    <div class="one-half">
      <h2 class="contact-title">Get In Touch</h2>
      <?php the_field('contact_info'); ?>
      <?php the_field('contact_form'); ?>
    </div>
    <div class="one-half">

    <?php 
      $location = get_field('contact_map');
      $location2 = get_field('contact_map_two');
      $location_one = preg_split("/,/",$location['address']);
      $location_two = preg_split("/,/",$location2['address']);
    ?>

    <?php if ( !empty($location2) ) { ?>

      <?php if( !empty($location) ) { ?>
        <div class="map-container split first">
          <a href="https://www.google.com/maps/place/<?php echo $location['address']; ?>" target="_blank" class="map-address">
            <strong><?php echo $location_one[0]; ?></strong><br>
            <strong><?php echo $location_one[1]; ?></strong>,<strong><?php echo $location_one[2]; ?></strong>
          </a>
          <div class="acf-map acf-map-one">
            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
          </div>
        </div>
      <?php } ?>
      <?php if( !empty($location2) ) { ?>
        <div class="map-container split">
          <a href="https://www.google.com/maps/place/<?php echo $location2['address']; ?>" target="_blank"  class="map-address">
            <strong><?php echo $location_two[0]; ?></strong><br>
            <strong><?php echo $location_two[1]; ?></strong>,<strong><?php echo $location_two[2]; ?></strong>
          </a>
          <div class="acf-map acf-map-two">
            <div class="marker" data-lat="<?php echo $location2['lat']; ?>" data-lng="<?php echo $location2['lng']; ?>"></div>
          </div>
        </div>
      <?php } ?>

    <?php } else { ?>
      <?php if( !empty($location) ) { ?>
        <div class="map-container">
          <a href="https://www.google.com/maps/place/<?php echo $location['address']; ?>" target="_blank" class="map-address">
            <strong><?php echo $location_one[0]; ?></strong><br>
            <strong><?php echo $location_one[1]; ?></strong>,<strong><?php echo $location_one[2]; ?></strong>
          </a>
          <div class="acf-map">
            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
          </div>
        </div>
      <?php } ?>
    <?php } ?>


    </div>
  </div>
</div>