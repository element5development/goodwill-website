<?php /*
DISPLAY SLIDER FOR GREENWORKS
*/ ?>


<?php if( have_rows('auto_slides') ) { ?>
  <section class="auto-slider">
    <a id="auto" class="page-anchor"></a>
    <div class="section-title-container grey-bg">
      <svg xmlns="http://www.w3.org/2000/svg" width="260" height="64" viewBox="0 0 260 64" style="max-width:100px; height: 30px; margin-bottom: 1rem;">
        <path d="M24.22 16.44h17.78v20.14h-5.36l-1.42-3.22c-3.24 3.22-7.32 4.46-14.5 4.46-8.78 0-20.72-2.42-20.72-18.76 0-15.5 11.08-19.06 20.68-19.06 13.92 0 20.52 4.26 21.5 13.36h-14.1c-.36-1.82-1.3-2.96-2.44-3.6-1.2-.66-2.62-.84-4.26-.84-5.1 0-8.34 2.82-8.34 9.98 0 6.54 3.1 10 9.32 10 4.08 0 6.12-1.78 7-4.44h-5.14zm39.74 20.82c12.5 0 17.96-4.66 17.96-14.66 0-9.94-5.46-14.6-17.96-14.6s-17.96 4.66-17.96 14.6c0 10 5.46 14.66 17.96 14.66zm0-21.24c4.48 0 6.36 2.32 6.36 6.58 0 4.36-1.88 6.64-6.36 6.64-4.44 0-6.34-2.28-6.34-6.64 0-4.26 1.9-6.58 6.34-6.58zm38 21.24c12.52 0 17.96-4.66 17.96-14.66 0-9.94-5.44-14.6-17.96-14.6-12.5 0-17.96 4.66-17.96 14.6 0 10 5.46 14.66 17.96 14.66zm0-21.24c4.48 0 6.34 2.32 6.34 6.58 0 4.36-1.86 6.64-6.34 6.64-4.44 0-6.34-2.28-6.34-6.64 0-4.26 1.9-6.58 6.34-6.58zm45.9 20.56h10.74v-36.58h-11.1v12.58h-.08c-1.92-2.58-5.1-3.82-9.64-3.82-7.18 0-13.78 4.4-13.78 14.46 0 10.04 6.6 14.46 13.78 14.46 4.92 0 8-1.4 9.98-3.96h.1zm-6.44-7.22c-4.28 0-6.08-2.58-6.08-6.14 0-3.46 2.06-6.14 6.04-6.14 4.3 0 6.12 2.32 6.12 6.14 0 3.86-1.82 6.14-6.08 6.14zm18.58-19.36h11.84l3.96 16h.08l5.28-16h9.4l4.84 16h.08l4.4-16h11.84l-8.26 27.08h-12.9l-4.7-14.16-4.7 14.16h-12.9zm54-3.16h11.08v-6.84h-11.08zm0 2.66h11.08v27.08h-11.08zm18 27.08h11.08v-36.58h-11.08zm16 0h11.08v-36.58h-11.08z" fill="#00539f"/>
        <path d="M2 63.84h257.78v-21.84h-257.78z" fill="#f05a28"/>
        <path d="M18.78 50.48l-3.1 4.24h6.18zm.22-2.48l8.46 11.7h-1.96l-2.84-3.86h-7.82l-2.82 3.86h-2.02l8.56-11.7zm21 0h1.82v7.06c0 .84.02 1.36.06 1.56.1.46.3.86.64 1.16.32.32.82.58 1.5.78.7.22 1.38.32 2.06.32.6 0 1.18-.08 1.74-.26.54-.16 1-.38 1.38-.68.36-.28.64-.64.8-1.06.12-.28.2-.9.2-1.82v-7.06h1.8v7.06c0 1.04-.16 1.88-.48 2.54-.3.64-.94 1.2-1.88 1.68-.96.48-2.1.7-3.46.7-1.46 0-2.7-.22-3.76-.66-1.04-.46-1.74-1.06-2.08-1.8-.22-.46-.34-1.28-.34-2.46zm22 1.14v-1.14h9.94v1.14h-4.06v10.56h-1.84v-10.56zm27.7-1.32c-1.2.44-2.14 1.04-2.82 1.82-.68.76-1 1.6-1 2.54 0 1.4.74 2.58 2.24 3.52 1.48.96 3.28 1.44 5.4 1.44 1.4 0 2.7-.22 3.9-.66 1.2-.44 2.12-1.04 2.8-1.8.66-.76 1-1.62 1-2.54 0-.94-.34-1.78-1-2.52-.68-.76-1.62-1.36-2.84-1.8-1.2-.46-2.5-.68-3.86-.68-1.34 0-2.62.22-3.82.68zm10.62-.04c1.86 1.18 2.78 2.62 2.78 4.36 0 1.7-.92 3.16-2.76 4.36-1.86 1.18-4.1 1.78-6.76 1.78-2.68 0-4.96-.6-6.8-1.78-1.86-1.18-2.78-2.62-2.78-4.3 0-1.14.42-2.18 1.26-3.14.84-.96 2-1.7 3.44-2.24 1.46-.54 3.04-.82 4.74-.82 2.74 0 5.04.6 6.88 1.78zm13.68 11.92l2.6-11.7h.28l7.38 9.6 7.3-9.6h.28l2.62 11.7h-1.78l-1.8-8.36-6.4 8.36h-.46l-6.48-8.44-1.78 8.44zm37.7-11.88c-1.2.44-2.14 1.04-2.82 1.82-.66.76-1 1.6-1 2.54 0 1.4.74 2.58 2.24 3.52 1.48.96 3.28 1.44 5.4 1.44 1.4 0 2.7-.22 3.9-.66 1.2-.44 2.12-1.04 2.8-1.8.68-.76 1-1.62 1-2.54 0-.94-.32-1.78-1-2.52-.68-.76-1.62-1.36-2.84-1.8-1.22-.46-2.5-.68-3.86-.68-1.36 0-2.62.22-3.82.68zm10.62-.04c1.86 1.18 2.78 2.62 2.78 4.36 0 1.7-.92 3.16-2.76 4.36-1.86 1.18-4.1 1.78-6.76 1.78-2.68 0-4.96-.6-6.8-1.78-1.86-1.18-2.78-2.62-2.78-4.3 0-1.14.42-2.18 1.26-3.14.84-.96 2-1.7 3.44-2.24 1.46-.54 3.04-.82 4.74-.82 2.74 0 5.04.6 6.88 1.78zm13.68 1.36v-1.14h9.94v1.14h-4.04v10.56h-1.86v-10.56zm22 10.56h1.82v-11.7h-1.82zm14-11.7h1.98l6 9.04 6.1-9.04h1.98l-7.88 11.7h-.4zm28 0h10.38v1.14h-8.56v3.66h8.5v1.16h-8.5v4.6h8.5v1.14h-10.32z" fill="#fefefe"/>
      </svg>
      <?php if ( get_field('auto_section_title') ) { ?>
        <h2><?php the_field('auto_section_title') ?></h2>
      <?php } ?>
      <?php if ( get_field('auto_section_subheading') ) { ?>
        <p class="section_subheading"><?php the_field('auto_section_subheading') ?></p>
      <?php } ?>
    </div>
    <div class="slider-container full-width dark-bg">
      <?php while ( have_rows('auto_slides') ) : the_row(); ?>

        <div class="single-slide">
          <?php $image = get_sub_field('slide_image'); ?>
          <div class="slide-image one-half full" style="background-image: url('<?php echo $image['url']; ?>');"></div>
          <div class="slide-content one-half full vertical-align-parent dark-bg">
            <div class="vertical-align-child">
              <h2><?php the_sub_field('slide_title'); ?></h2>
              <?php if ( get_sub_field('slide_description') ) { ?>
                <h3><?php the_sub_field('slide_description'); ?></h3>
              <?php } ?>
              <?php if ( get_sub_field('cta_url') ) { ?>
                <a href="<?php the_sub_field('cta_url'); ?>" class="btn primary-btn icon"><span><?php the_sub_field('cta_text'); ?></span></a>
              <?php } ?>
            </div>
          </div>
        </div>

      <?php endwhile; ?>
    </div>
  </section>
<?php } else { 
    // NO SLIDES
} ?>
