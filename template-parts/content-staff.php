<?php /*
DISPLAY 3 LATEST SUCCESSTORIES
*/ ?>


<section class="staff-container grey-bg full-width">
  <a id="staff" class="page-anchor"></a>
  <div class="max-width">

    <h2>Board of Directors</h2>
    <?php //QUERY ALL DIRECTORS
      $args = array( 
        'posts_per_page'  => -1, 
        'post_type' => 'staff',
        'meta_query'=> array(
          array(
            'key'=>'type',
            'value'=> 'director',
            'compare' => '='
          )
        ),
        'order'       => 'DESC' 
      );
      $query = new WP_Query( $args );
    ?>
    <?php if ( $query->have_posts() ) { ?>
      <div class="staff-preview-container flex-container">
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
          <div class="staff-preview director one-third">
            <div class="contents">
              <p class="position"><?php the_field('position'); ?></p>
              <div class="details">
                <h3 class="name"><?php the_title(); ?> <?php the_field('last_name') ?></h3>
                <p class="title"><?php the_field('title'); ?></p>
                <span class="company"><?php the_field('company'); ?></span>
              </div>
            </div>
            <?php if ( get_field('linkedin_url') ) { ?>
              <a href="<?php the_field('linkedin_url'); ?>" target="_blank" class="linkedin social-link">
                <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.117 430.117" enable-background="new 0 0 430.117 430.117">
                  <path d="M430.117 261.543v159.017h-92.188v-148.367c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477v154.873h-92.219s1.242-251.285 0-277.32h92.21v39.309l-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zm-377.934-251.985c-31.548 0-52.183 20.693-52.183 47.905 0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zm-46.706 411.002h92.184v-277.32h-92.184v277.32z"></path>
                </svg>
                <span>LinkedIn Profile</span>
              </a>
            <?php } ?>
          </div>
        <?php endwhile; ?>
      </div>
    <?php } ?>
    <?php wp_reset_postdata(); ?>

    <h2>Board Members</h2>
    <?php //QUERY ALL DIRECTORS
      $args = array( 
        'posts_per_page'  => -1, 
        'post_type' => 'staff',
        'meta_query'=> array(
          array(
            'key'=>'type',
            'value'=> 'member',
            'compare' => '='
          )
        ),
        'meta_key'      => 'last_name',
        'orderby'     => 'meta_value',
        'order'       => 'ASC' 
        );
      $query = new WP_Query( $args );
    ?>
    <?php if ( $query->have_posts() ) { ?>
      <div class="staff-preview-container flex-container">
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
          <div class="staff-preview member one-third">
            <div class="contents">
              <h3 class="name"><?php the_title(); ?> <?php the_field('last_name') ?></h3>
              <p class="title"><?php the_field('title'); ?></p>
              <span class="company"><?php the_field('company'); ?></span>
            </div>
            <?php if ( get_field('linkedin_url') ) { ?>
              <a href="<?php the_field('linkedin_url'); ?>" target="_blank" class="linkedin social-link">
                <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.117 430.117" enable-background="new 0 0 430.117 430.117">
                  <path d="M430.117 261.543v159.017h-92.188v-148.367c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477v154.873h-92.219s1.242-251.285 0-277.32h92.21v39.309l-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zm-377.934-251.985c-31.548 0-52.183 20.693-52.183 47.905 0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zm-46.706 411.002h92.184v-277.32h-92.184v277.32z"></path>
                </svg>
                <span>LinkedIn Profile</span>
              </a>
            <?php } ?>
          </div>
        <?php endwhile; ?>
      </div>
    <?php } ?>
    <?php wp_reset_postdata(); ?>

    <h2>Executive Staff</h2>
        <?php //QUERY ALL DIRECTORS
      $args = array( 
        'posts_per_page'  => -1, 
        'post_type' => 'staff',
        'meta_query'=> array(
          array(
            'key'=>'type',
            'value'=> 'executive',
            'compare' => '='
          )
        ),
        'meta_key'      => 'last_name',
        'orderby'     => 'meta_value',
        'order'       => 'ASC' 
      );
      $query = new WP_Query( $args );
    ?>
    <?php if ( $query->have_posts() ) { ?>
      <div class="staff-preview-container flex-container">
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
          <div class="card-overlay one-third">
            <?php $image = get_field('headshot'); ?>
            <div class="card-img" style="background-image: url('<?php echo $image['url']; ?>');"></div>
            <div class="card-info">
              <div class="card-headshot" style="background-image: url('<?php echo $image['url']; ?>');"></div>
              <div class="details">
                <h3 class="name"><?php the_title(); ?> <?php the_field('last_name') ?></h3>
                <p class="title"><?php the_field('title'); ?></p>
              </div>
              <div class="bio"><?php the_field('bio'); ?></div>
              <a href="javascript:void(0);" class="tab-focus">
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                  <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                </svg>
                <span>Open Biography</span>
              </a>
            </div>
          </div>
        <?php endwhile; ?>
      </div>
    <?php } ?>
    <?php wp_reset_postdata(); ?>

  </div>
</section>