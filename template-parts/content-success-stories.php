<?php /*
DISPLAY 3 LATEST SUCCESSTORIES
*/ ?>

<?php //2 LATEST POSTS
  $args = array( 
    'posts_per_page'  => 3, 
    'post_type' => 'success-stories',
    'orderby' => 'rand'
    );
  $query = new WP_Query( $args );
?>

<?php if ( $query->have_posts() ) { ?>
  <section class="success-stories card-container full-width">
    <h2>Success Stories</h2>
    <p class="section_subheading">You can be our next story.</p>
    <div class="max-width flex-container">

      <?php while ( $query->have_posts() ) : $query->the_post(); ?>
        <a href="<?php echo get_permalink(505); ?>" class="flip-container card-flip one-third">
          <div class="flipper">
            <div class="front">
              <?php $image = get_field('featured_image'); ?>
              <div class="card-img" style="background-image: url('<?php echo $image['url']; ?>');"></div>
              <h3><?php the_title(); ?></h3>
              <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
              </svg>
            </div>
            <div class="back">
              <blockquote><p><?php the_field('quote') ?></p></blockquote>
              <div class="learn-more btn primary-btn icon"><span>Learn More</span>
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                  <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                </svg>
              </div>
            </div>
          </div>
        </a>
      <?php endwhile; ?>
    </div>
  </section>
<?php } ?>
<?php wp_reset_postdata(); ?>