<?php /*
Template Name: Sidebar
*/ ?>


<?php get_header(); ?>

<main class="page-contents-container full-width <?php if ( is_page(393) ) { ?>grey-bg<?php } ?>">

  <!-- PAGE TITLE -->
  <?php get_template_part( 'template-parts/content', 'page-title' ); ?>

  <!--SKIP NAV -->
  <a id="main-anchor" class="page-anchor"></a>

  <div class="sidebar-template max-width flex-container">
    <div class="page-main-contents">
    
      <!-- SECONDARY NAVIGATION -->
      <?php get_template_part( 'template-parts/content', 'secondary-nav' ); ?>

      <!-- ADD PAGE CONTENT -->
      <?php if ( get_the_content() ) { ?>
        <div class="editor-contents max-width">
          <a id="editor-contents" class="page-anchor"></a>
          <?php the_content(); ?>
        </div>
      <?php } else if ( is_page(511) ) { //FLI THE SCRIPT?>
        <div class="editor-contents max-width">
          <a id="editor-contents" class="page-anchor"></a>
          <h2>The Program</h2>
          <?php get_template_part( 'template-parts/content', 'script-cards' ); ?>
        </div>
        <div class="impact max-width">
          <a id="impact" class="page-anchor"></a>
          <h2 style="margin-bottom: .5rem;">Impact Data</h2>
          <p><?php the_field('impact_intro') ?></p>
          <?php get_template_part( 'template-parts/content', 'script-impact' ); ?>
        </div>
        <div class="success max-width">
          <a id="success" class="page-anchor"></a>
          <h2>Flip the Script Success Stories</h2>
          <?php get_template_part( 'template-parts/content', 'script-success' ); ?>
        </div>
      <?php } ?>

      <!-- CARDS ICON -->
      <?php if( have_rows('icon_cards') ) { ?>
        <?php get_template_part( 'template-parts/content', 'cards-icon' ); ?>
      <?php } ?>

      <!-- CARDS CTA -->
      <?php if ( have_rows('cta_cards') ) { ?>
        <?php get_template_part( 'template-parts/content', 'cards-cta' ); ?>
      <?php } ?>

      <!-- CARDS FLIP -->
      <?php if ( have_rows('flip_cards')) { ?>
        <?php get_template_part( 'template-parts/content', 'cards-flip' ); ?>
      <?php } ?>

      <!-- SINGLE SERVICE CONTACT FORM -->
      <?php if ( 507 == $post->post_parent ) { ?> 
        <?php get_template_part( 'template-parts/content', 'service-contact' ); ?>
      <?php } ?>

      <!--EVENTS LIST -->
      <?php if ( is_page(393) ) { ?>
        <?php get_template_part( 'template-parts/content', 'events' ); ?>
      <?php } ?>
    </div>
    <aside>
      <?php if ( is_page(393) ) { ?>
        <?php get_sidebar('events'); ?>
      <?php } else { ?>
        <?php get_sidebar('services'); ?>
      <?php } ?>
    </aside>
  </div>

  <!-- CONTENT WITH BACKGROUND IMAGE -->
  <?php if ( get_field('content_background_image') ) { //ABOUT ?>
    <?php get_template_part( 'template-parts/content', 'content-bg' ); ?>
  <?php } ?>

  <!-- SUCCESS STORIES QUERY -->
  <?php if ( !is_page(390) && !is_page(391) && 507 != $post->post_parent ) { ?>
    <?php get_template_part( 'template-parts/content', 'success-stories' ); ?>
  <?php } ?>

  <!-- BACK TO TOP -->
  <a href="#main-anchor" class="back-to-top skip smoothScroll hidden">
    <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
      <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
    </svg>
  </a>

</main>

<?php get_footer(); ?>