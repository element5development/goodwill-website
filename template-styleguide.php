<?php /*
Template Name: Styleguide
*/ ?>

<?php get_header(); ?>

<main class="full-width">
  <div class="styleguide-container max-width clearfix">
    <div id="styleguide-sidebar" class="sidebar left">
      <nav>
        <ul class="menu">
          <li><a class="smoothScroll" href="#styleguide-logo">Logos</a></li>
          <li><a class="smoothScroll" href="#styleguide-colors">Colors</a></li>
          <li><a class="smoothScroll" href="#styleguide-typography">Typography</a></li>
          <li><a class="smoothScroll" href="#styleguide-buttons">Buttons</a></li>
          <li><a class="smoothScroll" href="#styleguide-forms">Forms</a></li>
          <?php if( have_rows('icons') ) { ?><li><a class="smoothScroll" href="#styleguide-icons">Icons</a></li><?php } ?>
          <?php if( have_rows('patterns') ) { ?><li><a class="smoothScroll" href="#styleguide-patterns">Patterns</a></li><?php } ?>
          <li><a class="smoothScroll" href="#styleguide-elements">Web Elements</a></li>
        </ul>
      </nav> 
    </div>
    <div id="styleguide-contents" class="sidebar-page-contents">

      <section class="logo-section">
        <a id="styleguide-logo" class="styleguide-anchor"></a>
        <h2>Logo</h2>
        <nav class="sub-section-nav">
          <ul>
            <li><a href="#primary-logo" class="smoothScroll">Primary</a></li>
            <li><a href="#variant-logos" class="smoothScroll">Variants</a></li>
          </ul>
        </nav>
        <div class="primary-logo styleguide-sub-section clearfix full-width"> 
          <a id="primary-logo" class="styleguide-anchor"></a> 
          <h3>Primary Logo</h3>
            <div class="full-width">
              <a target="_blank" href="<?php the_field('primary_logo'); ?>"><img src="<?php the_field('primary_logo'); ?>" /></a>
            </div>
        </div>
        <?php if( have_rows('logo_variant') ) { $i = 0; ?>
          <div class="variant-logos styleguide-sub-section clearfix full-width"> 
            <a id="variant-logos" class="styleguide-anchor"></a> 
            <h3>Variants</h3>
            <p class="legal-text">*Click to download any logo</p>
            <div class="flex-container">
              <?php while ( have_rows('logo_variant') ) : the_row(); $i++; ?>
                <div class="one-half">
                  <div class="varient-image vertical-align-parent" style="background-color: <?php the_sub_field('background_color'); ?>">
                    <div class="vertical-align-child">
                      <a target="_blank" href="<?php the_sub_field('logo'); ?>"><img src="<?php the_sub_field('logo'); ?>" /></a>
                    </div>
                  </div>
                  <div class="varient-description">
                    <p class="legal-text"><?php the_sub_field('description'); ?></p>
                  </div>
                </div>
                <?php if ( $i % 2 == 0 ) { ?>
                  <div style="clear: both"></div>
                <?php } ?>
              <?php endwhile; ?>
            </div>
          </div>
        <?php } else {
          //NOTHING
        } ?>
      </section>

      <section class="styleguide-section">
        <a id="styleguide-colors" class="styleguide-anchor"></a>
        <h2>Colors</h2>
        <nav class="sub-section-nav">
          <ul>
            <li><a href="#primary-colors" class="smoothScroll">Primary</a></li>
            <li><a href="#secondary-colors" class="smoothScroll">Secondary</a></li>
          </ul>
        </nav>

        <?php if( have_rows('primary_colors') ) { ?>
          <div class="primary-colors styleguide-sub-section clearfix full-width"> 
            <a id="primary-colors" class="styleguide-anchor"></a> 
            <h3>Primary Colors</h3>
            <div class="flex-container">
              <?php while ( have_rows('primary_colors') ) : the_row(); ?>
                <div class="one-fourth">
                  <div class="color-swatch" style="background-color: <?php the_sub_field('color_hex_code'); ?>;"></div>
                  <div class="color-swatch-details">
                    <?php
                      $rgb = hex2rgb( get_sub_field('color_hex_code') );
                      $cmyk = rgb2cmyk(hex2rgb2( get_sub_field('color_hex_code') ));
                    ?>
                    <p><b><?php the_sub_field('color_name'); ?></b></p>
                    <p class="legal-text"><?php the_sub_field('color_hex_code'); ?> | <?php the_sub_field('color_pantone'); ?></p>
                    <p class="legal-text"><?php echo $rgb; ?></p>
                    <p class="legal-text"><?php echo $cmyk; ?></p>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>
          </div>
        <?php } else {
          //NOTHING
        } ?>

        <?php if( have_rows('secondary_colors') ) { ?>
          <div class="secondary-colors styleguide-sub-section clearfix full-width"> 
            <a id="secondary-colors" class="styleguide-anchor"></a> 
            <h3>Secondary Colors</h3>
            <div class="flex-container">
              <?php while ( have_rows('secondary_colors') ) : the_row(); ?>
                <div class="one-fourth">
                  <div class="color-swatch" style="background-color: <?php the_sub_field('color_hex_code'); ?>;"></div>
                  <div class="color-swatch-details">
                    <?php
                      $rgb = hex2rgb( get_sub_field('color_hex_code') );
                      $cmyk = rgb2cmyk(hex2rgb2( get_sub_field('color_hex_code') ));
                    ?>
                    <p><b><?php the_sub_field('color_name'); ?></b></p>
                    <p class="legal-text"><?php the_sub_field('color_hex_code'); ?> | <?php the_sub_field('color_pantone'); ?></p>
                    <p class="legal-text"><?php echo $rgb; ?></p>
                    <p class="legal-text"><?php echo $cmyk; ?></p>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>
          </div>
        <?php } else {
          //NOTHING
        } ?>
      </section>

      <section class="styleguide-section">
        <a id="styleguide-typography" class="styleguide-anchor"></a>
        <h2>Typography</h2>
        <nav class="sub-section-nav">
          <ul>
            <li><a href="#typeface" class="smoothScroll">Typeface</a></li>
            <li><a href="#headlines" class="smoothScroll">Headlines</a></li>
            <li><a href="#paragraph" class="smoothScroll">Paragraph</a></li>
            <li><a href="#text-formatting" class="smoothScroll">Text Formatting</a></li>
          </ul>
        </nav>

        <?php if( have_rows('typeface') ) { ?>
          <div class="typeface styleguide-sub-section clearfix full-width"> 
            <a id="typeface" class="styleguide-anchor"></a> 
            <h3>Typeface</h3>
            <div class="flex-container">
              <?php while ( have_rows('typeface') ) : the_row(); ?>
                <div class="one-fourth vertical-align-parent">
                  <div class="typeface-option vertical-align-child">
                    <p><span style="
                    <?php
                      //ITALIC?
                      if ( get_sub_field('font_italic') == 'Yes' ) {
                        echo "font-style: italic; ";
                      } else {
                        echo "font-style: none; ";
                      }
                      //FONT WEIGHT
                      echo "font-weight: ";
                      the_sub_field('font_weight');
                      echo ";";
                    ?>
                    ">Aa Zz</span></p>
                    <p class="legal-text"><?php the_sub_field('font_name'); ?></p>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>
          </div>
        <?php } else {
          //NOTHING
        } ?>

        <div class="heaadlines styleguide-sub-section clearfix full-width"> 
          <a id="headlines" class="styleguide-anchor"></a> 
          <h3>Headlines</h3>
          <div class="headline-option <?php if ( get_field('h1_color', 'option') == '#FFFFFF' || get_field('h1_color', 'option') == '#ffffff') { echo 'dark-bg'; } ?>" >
            <h1>Heading One</h1>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h1_font_size', 'option');
                $lh = ( $font * get_field('h1_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h1_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h1_color', 'option') ));
              ?>
              <?php the_field('h1_font_size', 'option'); ?>em/<?php the_field('h1_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h1_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h1_character_spacing', 'option'); ?>px 
              | <?php the_field('h1_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option <?php if ( get_field('h2_color', 'option') == '#FFFFFF' || get_field('h2_color', 'option') == '#ffffff') { echo 'dark-bg'; } ?>">
            <h2>Heading Two</h2>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h2_font_size', 'option');
                $lh = ( $font * get_field('h2_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h2_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h2_color', 'option') ));
              ?>
              <?php the_field('h2_font_size', 'option'); ?>em/<?php the_field('h2_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h2_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h2_character_spacing', 'option'); ?>px 
              | <?php the_field('h2_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option <?php if ( get_field('h3_color', 'option') == '#FFFFFF' || get_field('h3_color', 'option') == '#ffffff') { echo 'dark-bg'; } ?>">
            <h3>Heading Three</h3>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h3_font_size', 'option');
                $lh = ( $font * get_field('h3_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h3_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h3_color', 'option') ));
              ?>
              <?php the_field('h3_font_size', 'option'); ?>em/<?php the_field('h3_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h3_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h3_character_spacing', 'option'); ?>px 
              | <?php the_field('h3_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option <?php if ( get_field('h4_color', 'option') == '#FFFFFF' || get_field('h4_color', 'option') == '#ffffff') { echo 'dark-bg'; } ?>">
            <h4>Heading Four</h4>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h4_font_size', 'option');
                $lh = ( $font * get_field('h4_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h4_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h4_color', 'option') ));
              ?>
              <?php the_field('h4_font_size', 'option'); ?>em/<?php the_field('h4_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h4_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h4_character_spacing', 'option'); ?>px 
              | <?php the_field('h4_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option <?php if ( get_field('h5_color', 'option') == '#FFFFFF' || get_field('h5_color', 'option') == '#ffffff') { echo 'dark-bg'; } ?>">
            <h5>Heading Five</h5>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h5_font_size', 'option');
                $lh = ( $font * get_field('h5_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h5_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h5_color', 'option') ));
              ?>
              <?php the_field('h5_font_size', 'option'); ?>em/<?php the_field('h5_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h5_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h5_character_spacing', 'option'); ?>px 
              | <?php the_field('h5_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option <?php if ( get_field('h6_color', 'option') == '#FFFFFF' || get_field('h6_color', 'option') == '#ffffff') { echo 'dark-bg'; } ?>">
            <h6>Heading Six</h6>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h6_font_size', 'option');
                $lh = ( $font * get_field('h6_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h6_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h6_color', 'option') ));
              ?>
              <?php the_field('h6_font_size', 'option'); ?>em/<?php the_field('h6_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h6_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h6_character_spacing', 'option'); ?>px 
              | <?php the_field('h6_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option">
            <p>Body Content</p>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('p_font_size', 'option');
                $lh = ( $font * get_field('p_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('p_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('p_color', 'option') ));
              ?>
              <?php the_field('p_font_size', 'option'); ?>em/<?php the_field('p_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('p_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('p_character_spacing', 'option'); ?>px 
              | <?php the_field('p_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option">
            <p class="block-quote">Block Quote</p>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('bq_font_size', 'option');
                $lh = ( $font * get_field('bq_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('bq_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('bq_color', 'option') ));
              ?>
              <?php the_field('bq_font_size', 'option'); ?>em/<?php the_field('bq_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('bq_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('bq_character_spacing', 'option'); ?>px 
              | <?php the_field('bq_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
        </div>

        <div class="paragraph styleguide-sub-section clearfix full-width"> 
          <a id="paragraph" class="styleguide-anchor"></a> 
          <h3>Paragraph</h3>
          <h2>Headline</h2>
          <p>Cras mattis consectetur purus sit amet fermentum. Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Etiam porta sem malesuada magna mollis euismod. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
          <h4>Subheadline</h4>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Curabitur blandit tempus porttitor. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Aenean lacinia bibendum nulla sed.</p>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,</p>
        </div>

        <div class="paragraph styleguide-sub-section clearfix full-width"> 
          <a id="text-formatting" class="styleguide-anchor"></a> 
          <h3>Text Formatting</h3>
          <?php the_field('text_formatting'); ?>
        </div>
      </section>

      <section class="styleguide-section">
        <a id="styleguide-buttons" class="styleguide-anchor"></a>
        <h2>Buttons</h2>
        <nav class="sub-section-nav">
          <ul>
            <li><a href="#p-buttons" class="smoothScroll">Basic Buttons</a></li>
            <li><a href="#secondary-nav" class="smoothScroll">Secondary Nav</a></li>
            <li><a href="#social-icons" class="smoothScroll">Social</a></li>
          </ul>
        </nav>

        <div class="p-buttons styleguide-sub-section clearfix full-width"> 
          <a id="p-buttons" class="styleguide-anchor"></a> 
          <h3>Basic Button</h3>
          <div class="button-continer">
            <a href="/styleguide" class="btn primary-btn"><span>Basic Button</span></a>
            <a href="/styleguide" class="btn primary-btn icon"><span>Icon Button</span></a>
            <a href="/styleguide" class="btn primary-btn ghost"><span>Ghost Button</span></a>
          </div>
          <div class="dark-bg button-continer">
            <a href="/styleguide" class="btn primary-btn"><span>Basic Button</span></a>
            <a href="/styleguide" class="btn primary-btn icon"><span>Icon Button</span></a>
            <a href="/styleguide" class="btn primary-btn ghost"><span>Ghost Button</span></a>
          </div>
        </div>

        <div class="secondary-nav styleguide-sub-section clearfix full-width">
          <a id="secondary-nav" class="styleguide-anchor"></a>
          <h3>Secondary Nav</h3>
          <nav class="secondary-nav">
            <ul>
              <li class="menu-item"><a href="#secondary-nav" class="smoothScroll">Option One</a></li>
              <li class="menu-item"><a href="#secondary-nav" class="smoothScroll">Option Two</a></li>
              <li class="menu-item"><a href="#secondary-nav" class="smoothScroll">Option Three</a></li>
              <li class="menu-item"><a href="#secondary-nav" class="smoothScroll">Option Four</a></li>
              <li class="menu-item"><a href="#secondary-nav" class="smoothScroll">Option Five</a></li>
            </ul>
          </nav>
        </div>

        <div class="paragraph styleguide-sub-section clearfix full-width"> 
          <a id="social-icons" class="styleguide-anchor"></a> 
          <h3>Social Icons</h3>
          <div class="social-links">
            <a href="/styleguide" class="facebook social-link">
              <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.113 430.114" enable-background="new 0 0 430.113 430.114">
                <path d="M158.081 83.3v59.218h-43.385v72.412h43.385v215.183h89.122v-215.177h59.805s5.601-34.721 8.316-72.685h-67.784v-49.511c0-7.4 9.717-17.354 19.321-17.354h48.557v-75.385h-66.021c-93.519-.005-91.316 72.479-91.316 83.299z"></path>
              </svg>
            </a>
            <a href="/styleguide" class="instagram social-link">
              <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 169.063 169.063" enable-background="new 0 0 169.063 169.063">
                <path d="M122.406 0h-75.752c-25.725 0-46.654 20.93-46.654 46.655v75.752c0 25.726 20.929 46.655 46.654 46.655h75.752c25.727 0 46.656-20.93 46.656-46.655v-75.752c.001-25.725-20.929-46.655-46.656-46.655zm31.657 122.407c0 17.455-14.201 31.655-31.656 31.655h-75.753c-17.454.001-31.654-14.2-31.654-31.655v-75.752c0-17.454 14.2-31.655 31.654-31.655h75.752c17.455 0 31.656 14.201 31.656 31.655v75.752zm-69.532-81.437c-24.021 0-43.563 19.542-43.563 43.563 0 24.02 19.542 43.561 43.563 43.561s43.563-19.541 43.563-43.561c0-24.021-19.542-43.563-43.563-43.563zm0 72.123c-15.749 0-28.563-12.812-28.563-28.561 0-15.75 12.813-28.563 28.563-28.563s28.563 12.813 28.563 28.563c0 15.749-12.814 28.561-28.563 28.561zm45.39-84.842c-2.89 0-5.729 1.17-7.77 3.22-2.051 2.04-3.23 4.88-3.23 7.78 0 2.891 1.18 5.73 3.23 7.78 2.04 2.04 4.88 3.22 7.77 3.22 2.9 0 5.73-1.18 7.78-3.22 2.05-2.05 3.22-4.89 3.22-7.78 0-2.9-1.17-5.74-3.22-7.78-2.04-2.05-4.88-3.22-7.78-3.22z"></path>
              </svg>
            </a>
            <a href="/styleguide" class="twitter social-link">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612" width="512" height="512" enable-background="new 0 0 612 612">
                <path d="M612 116.258c-22.525 9.981-46.694 16.75-72.088 19.772 25.929-15.527 45.777-40.155 55.184-69.411-24.322 14.379-51.169 24.82-79.775 30.48-22.907-24.437-55.49-39.658-91.63-39.658-69.334 0-125.551 56.217-125.551 125.513 0 9.828 1.109 19.427 3.251 28.606-104.326-5.24-196.835-55.223-258.75-131.174-10.823 18.51-16.98 40.078-16.98 63.101 0 43.559 22.181 81.993 55.835 104.479-20.575-.688-39.926-6.348-56.867-15.756v1.568c0 60.806 43.291 111.554 100.693 123.104-10.517 2.83-21.607 4.398-33.08 4.398-8.107 0-15.947-.803-23.634-2.333 15.985 49.907 62.336 86.199 117.253 87.194-42.947 33.654-97.099 53.655-155.916 53.655-10.134 0-20.116-.612-29.944-1.721 55.567 35.681 121.536 56.485 192.438 56.485 230.948 0 357.188-191.291 357.188-357.188l-.421-16.253c24.666-17.593 46.005-39.697 62.794-64.861z"></path>
              </svg>
            </a>
            <a href="/styleguide" class="linkedin social-link">
              <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.117 430.117" enable-background="new 0 0 430.117 430.117">
                <path d="M430.117 261.543v159.017h-92.188v-148.367c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477v154.873h-92.219s1.242-251.285 0-277.32h92.21v39.309l-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zm-377.934-251.985c-31.548 0-52.183 20.693-52.183 47.905 0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zm-46.706 411.002h92.184v-277.32h-92.184v277.32z"></path>
              </svg>
            </a>
          </div>
        </div>

      </section>

      <section class="styleguide-section">
        <a id="styleguide-forms" class="styleguide-anchor"></a>
        <h2>Forms</h2>
        <?php echo do_shortcode('[gravityform id="2" title="false" description="false"]'); ?>
      </section>

      <?php if( have_rows('icons') ) { ?>
        <section class="styleguide-section">
          <a id="styleguide-icons" class="styleguide-anchor"></a>
          <h2>Icons</h2>
          <div class="icons-container flex-container clearfix">
                <?php while ( have_rows('icons') ) : the_row(); ?>
                  <div class="one-fourth">
                    <div class="vertical-align-parent">
                      <div class="icon-container vertical-align-child">
                        <?php 
                          $image = get_sub_field('icon'); 
                          $thumb = $image['sizes'][ 'thumbnail' ];
                        ?>
                       <a targets="_blank" href="<?php echo $image['url']; ?>"><img src="<?php echo $thumb; ?>" /></a>
                      </div>
                    </div>
                  </div>
                <?php endwhile; ?>
          </div>
          <?php if ( get_field('icon_zip_download')) { ?>
            <a href="<?php the_field('icon_zip_download'); ?>" class="primary-button">Download All</a>
          <?php } ?>
          <p class="legal-text">*Click to download any icon</p>
        </section>
      <?php } ?>

      <?php if( have_rows('patterns') ) { ?>
        <section class="styleguide-section">
          <a id="styleguide-patterns" class="styleguide-anchor"></a>
          <h2>Patterns</h2>
          <div class="patterns-container clearfix">
                <?php while ( have_rows('patterns') ) : the_row(); ?>
                  <div class="full-width">
                    <div class="pattern-container">
                      <a target="_blank" href="<?php the_sub_field('pattern'); ?>"><img src="<?php the_sub_field('pattern'); ?>" /></a>
                    </div>
                  </div>
                <?php endwhile; ?>
          </div>
          <a href="<?php the_field('pattern_zip_download'); ?>" class="primary-button">Download All</a>
          <p class="legal-text">*Click to download any pattern</p>
        </section>
      <?php } ?>

      <section class="styleguide-section">
        <a id="styleguide-elements" class="styleguide-anchor"></a>
        <h2>Web Elements</h2>
        <nav class="sub-section-nav">
          <ul>
            <li><a href="#element-title" class="smoothScroll">Page Title</a></li>
            <li><a href="#element-slider" class="smoothScroll">Slider</a></li>
            <li><a href="#element-card-icon" class="smoothScroll">Icon Card</a></li>
            <li><a href="#element-card-cta" class="smoothScroll">CTA Card</a></li>
            <li><a href="#element-card-flip" class="smoothScroll">Flip Card</a></li>
            <li><a href="#element-preview-event" class="smoothScroll">Event Preview</a></li>
            <li><a href="#element-preview-job" class="smoothScroll">Job Preview</a></li>
          </ul>
        </nav>

        <div class="element-title styleguide-sub-section clearfix full-width"> 
          <a id="element-title" class="styleguide-anchor"></a> 
          <h3>Page Title</h3>
          <div class="page-title-container full-width vertical-align-parent dark-bg" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2017/03/page-titles-bg.jpg');">
            <div class="page-title-contents vertical-align-child">
              <div class="page-icon" style="background-image: url(http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg);"></div>
              <h1 class="page-title">Support something greater.</h1>
              <p class="page-description">YOUR DONATIONS OF MONEY AND DONATIONS OF GENTLY USED ITEMS TRULY HELP OUT THOSE IN NEED IN THE DETROIT COMMUNITY.</p>
              <a href="" class="btn primary-btn icon"><span>Donate Now</span></a>
            </div>
            <div class="page-title-overlay"></div>
          </div>
        </div>

        <div class="element-slider styleguide-sub-section clearfix full-width"> 
          <a id="element-slider" class="styleguide-anchor"></a> 
          <h3>Slider</h3>
          <div class="slider-container full-width dark-bg">
            <div class="single-slide">
              <div class="slide-content one-half full vertical-align-parent dark-bg">
                <div class="vertical-align-child">
                  <h2>Save money, reduce waste, make an impact.</h2>
                  <h3>Do you have industrial, manufacturing, construction or demolition waste? Do you want to save money? Are you environmentally conscious? Do you care about Detroit?</h3>
                  <a href="" class="btn primary-btn icon"><span>Donate Now</span></a>
                </div>
              </div>
              <div class="slide-image one-half full" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2017/03/page-titles-bg.jpg');"></div>
            </div>
            <div class="single-slide">
              <div class="slide-content one-half full vertical-align-parent dark-bg">
                <div class="vertical-align-child">
                  <h2>Lorem ipsum dolor sit amet</h2>
                  <h3>Donec rutrum pellentesque placerat. Morbi lobortis metus quis mauris semper, id lobortis metus placerat. Duis aliquam pharetra turpis non eleifend.</h3>
                </div>
              </div>
              <div class="slide-image one-half full" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2017/03/page-titles-bg.jpg');"></div>
            </div>
            <div class="single-slide">
              <div class="slide-content one-half full vertical-align-parent dark-bg">
                <div class="vertical-align-child">
                  <h2>Maecenas aliquam laoreet</h2>
                  <h3>Etiam tempus ipsum purus, eu tempor lacus condimentum consequat. Nulla facilisi. Vestibulum maximus risus non hendrerit consequat.</h3>
                  <a href="" class="btn primary-btn icon"><span>Donate Now</span></a>
                </div>
              </div>
              <div class="slide-image one-half full" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2017/03/page-titles-bg.jpg');"></div>
            </div>
          </div>
        </div>

        <div class="element-card-icon styleguide-sub-section clearfix full-width"> 
        <a id="element-card-icon" class="styleguide-anchor"></a> 
        <h3>Card Icon</h3>
          <div class="card-container grey-bg flex-container full-width">
            <div class="card-icon one-fourth">
              <div class="card-icon-label" style="background-image: url(http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg);"></div>
              <h3>Donations</h3>
              <a href="/" class="learn-more">Learn More
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                  <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                </svg>
              </a>
              <a href="/" class="card-link"></a>
            </div>
            <div class="card-icon one-fourth">
              <div class="card-icon-label" style="background-image: url(http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg);"></div>
              <h3>Services for Individuals</h3>
              <a href="/" class="learn-more">Learn More
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                  <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                </svg>
              </a>
              <a href="/" class="card-link"></a>
            </div>
            <div class="card-icon one-fourth">
              <div class="card-icon-label" style="background-image: url(http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg);"></div>
              <h3>Donations</h3>
              <a href="/" class="learn-more">Learn More
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                  <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                </svg>
              </a>
              <a href="/" class="card-link"></a>
            </div>
            <div class="card-icon one-fourth">
              <div class="card-icon-label" style="background-image: url(http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg);"></div>
              <h3>Donations</h3>
              <a href="/" class="learn-more">Learn More
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                  <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                </svg>
              </a>
              <a href="/" class="card-link"></a>
            </div>
          </div>
        </div>

        <div class="element-card-cta styleguide-sub-section clearfix full-width"> 
          <a id="element-card-cta" class="styleguide-anchor"></a> 
          <h3>Card CTA</h3>
          <div class="card-container flex-container full-width">
            <div class="card-cta one-third" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2017/03/page-titles-bg.jpg');">
              <div class="card-contents">
                <div class="card-icon-label vertical-align-parent">
                  <div class="vertical-align-child"><img src="http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg"/></div>
                </div>
                <h3>Goodwill Automotive</h3>
              </div>
              <a href="/" class="card-link"></a>
            </div>
            <div class="card-cta one-third" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2017/03/page-titles-bg.jpg');">
              <div class="card-contents">
                <div class="card-icon-label vertical-align-parent">
                  <div class="vertical-align-child"><img src="http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg"/></div>
                </div>
                <h3>Goodwill Greenworks</h3>
              </div>
              <a href="/" class="card-link"></a>
            </div>
            <div class="card-cta one-third" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2017/03/page-titles-bg.jpg');">
              <div class="card-contents">
                <div class="card-icon-label vertical-align-parent">
                  <div class="vertical-align-child"><img src="http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg"/></div>
                </div>
                <h3>Goodwill<br>Automotive</h3>
              </div>
              <a href="/" class="card-link"></a>
            </div>
          </div>
        </div>

        <div class="element-card-flip styleguide-sub-section clearfix full-width"> 
          <a id="element-card-flip" class="styleguide-anchor"></a> 
          <h3>Card Flip</h3>
          <div class="card-container card-flip-container flex-container full-width">
            <div class="flip-container card-flip one-third">
              <div class="flipper">
                <div class="front">
                  <div class="card-img" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2017/03/page-titles-bg.jpg');"></div>
                  <h3>I am a returning citizen</h3>
                  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                    <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                  </svg>
                </div>
                <div class="back">
                  <h3>This is the back</h3>
                  <p>Phasellus sit amet varius nisi, eget tempus dui. Aenean laoreet enim ut tellus ultricies, rutrum aliquet lacus luctus. Ut in condimentum orci, congue eleifend tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                  <a href="/" class="learn-more btn primary-btn icon"><span>Learn More</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                      <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
            <div class="flip-container card-flip one-third">
              <div class="flipper">
                <div class="front">
                  <div class="card-img" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2017/03/page-titles-bg.jpg');"></div>
                  <h3>I am a returning citizen</h3>
                  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                    <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                  </svg>
                </div>
                <div class="back">
                  <h3>This is the back</h3>
                  <p>Phasellus sit amet varius nisi, eget tempus dui. Aenean laoreet enim ut tellus ultricies, rutrum aliquet lacus luctus. Ut in condimentum orci, congue eleifend tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                  <a href="/" class="learn-more btn primary-btn icon"><span>Learn More</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                      <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
            <div class="flip-container card-flip one-third">
              <div class="flipper">
                <div class="front">
                  <div class="card-img" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2017/03/page-titles-bg.jpg');"></div>
                  <h3>I am a returning citizen</h3>
                  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                    <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                  </svg>
                </div>
                <div class="back">
                  <h3>This is the back</h3>
                  <p>Phasellus sit amet varius nisi, eget tempus dui. Aenean laoreet enim ut tellus ultricies, rutrum aliquet lacus luctus. Ut in condimentum orci, congue eleifend tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                  <a href="learn-more btn primary-btn icon" class="learn-more"><span>Learn More</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                      <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="element-preview-event styleguide-sub-section clearfix full-width"> 
          <a id="element-preview-event" class="styleguide-anchor"></a> 
          <h3>Event Preview</h3>
          <div class="card-container full-width">
            <div class="event-preview flex-container">
              <div class="event-date dark-bg">
                <p class="date">
                  <span>March</span>
                  <span>09</span>
                  <span>2017</span>
                </p>
                <a href="/" class="btn primary-btn icon"><span>Reserve Now</span></a>
              </div>
              <div class="event-contents">
                <div class="description">
                  <h3>Harmonies & Hope 2017 - Vanessa Williams</h3>
                  <p>“Harmonies & Hope” is a celebration of Detroit’s musical past and present, and an opportunity to raise funds in support of its bright economic future. This event brings together members of the Detroit community, and beyond, for a night of “good-will” and great music. Most importantly, this event helps put the people of Metro Detroit back to work.</p>
                  <a href="/" class="event-link"></a>
                </div>
                <a href="/" class="location"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="38" height="51" viewBox="0 0 38 51">
                  <path d="M12.54 18.81c0-3.45 2.82-6.27 6.27-6.27 3.48 0 6.3 2.82 6.3 6.27 0 3.48-2.82 6.3-6.3 6.3-3.45 0-6.27-2.82-6.27-6.3zm-6.27 0c0 6.96 5.61 12.57 12.54 12.57 6.96 0 12.57-5.61 12.57-12.57 0-6.93-5.61-12.54-12.57-12.54-6.93 0-12.54 5.61-12.54 12.54zm-6.27 0c0-10.38 8.43-18.81 18.81-18.81 10.41 0 18.84 8.43 18.84 18.81 0 10.41-18.84 31.38-18.84 31.38s-18.81-20.97-18.81-31.38z"></path>
                </svg>
                MotorCity Casino Hotel, 2901 Grand River Ave, Detroit, MI 48201
                </a>
                <div class="social-likes"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="element-preview-job styleguide-sub-section clearfix full-width"> 
          <a id="element-preview-job" class="styleguide-anchor"></a> 
          <h3>Job Preview</h3>
          <div class="card-container flex-container full-width">
            <div class="job-preview one-half">
              <div class="description flex-container">
                <h3>Program Coordinator STEM</h3>
                <div class="text-description">
                  <p>Under supervision of DIrector, responisble for coordination of STEM program services implementation to achieve grant outcomes for trainees enrolled…</p>
                </div>
                <div class="icon-description" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg');"></div>
                <a href="/" class="job-link"></a>
              </div>
              <div class="details flex-container">
                <div class="location-type">
                  <div class="location">
                    <svg xmlns="http://www.w3.org/2000/svg" width="38" height="51" viewBox="0 0 38 51">
                      <path d="M12.54 18.81c0-3.45 2.82-6.27 6.27-6.27 3.48 0 6.3 2.82 6.3 6.27 0 3.48-2.82 6.3-6.3 6.3-3.45 0-6.27-2.82-6.27-6.3zm-6.27 0c0 6.96 5.61 12.57 12.54 12.57 6.96 0 12.57-5.61 12.57-12.57 0-6.93-5.61-12.54-12.57-12.54-6.93 0-12.54 5.61-12.54 12.54zm-6.27 0c0-10.38 8.43-18.81 18.81-18.81 10.41 0 18.84 8.43 18.84 18.81 0 10.41-18.84 31.38-18.84 31.38s-18.81-20.97-18.81-31.38z"></path>
                    </svg>
                    Detroit, MI
                  </div>
                  <div class="type">
                    <svg xmlns="http://www.w3.org/2000/svg" width="43" height="43" viewBox="0 0 43 43">
                      <path d="M30.36 18.48c-1.11 0-1.98.87-1.98 1.98v13.86c0 1.08.87 1.98 1.98 1.98h3.96c1.08 0 1.98-.9 1.98-1.98v-13.86c0-1.11-.9-1.98-1.98-1.98zm-11.22-7.92c-1.11 0-1.98.87-1.98 1.98v21.78c0 1.08.87 1.98 1.98 1.98h3.96c1.08 0 1.98-.9 1.98-1.98v-21.78c0-1.11-.9-1.98-1.98-1.98zm-11.22 17.82c-1.08 0-1.98.87-1.98 1.98v3.96c0 1.08.9 1.98 1.98 1.98h3.96c1.08 0 1.98-.9 1.98-1.98v-3.96c0-1.11-.9-1.98-1.98-1.98zm-3.96-24.42h34.32v34.32h-34.32zm36.3 38.28c1.08 0 1.98-.9 1.98-1.98v-38.28c0-1.08-.9-1.98-1.98-1.98h-38.28c-1.08 0-1.98.9-1.98 1.98v38.28c0 1.08.9 1.98 1.98 1.98z"></path>
                    </svg>
                    Mid-level, Short Term
                  </div>
                </div>
                <a href="/" class="btn primary-btn icon"><span>APPLY FOR THIS</span></a>
              </div>
            </div>
            <div class="job-preview one-half">
              <div class="description flex-container">
                <h3>Vice President of Information Technology (IT) and Infrastructure</h3>
                <div class="text-description">
                  <p>Under supervision of DIrector, responisble for coordination of STEM program services implementation to achieve grant outcomes for trainees enrolled…</p>
                </div>
                <div class="icon-description" style="background-image: url('http://e5goodwill.wpengine.com/wp-content/uploads/2016/10/icon-donations.svg');"></div>
                <a href="/" class="job-link"></a>
              </div>
              <div class="details flex-container">
                <div class="location-type">
                  <div class="location">
                    <svg xmlns="http://www.w3.org/2000/svg" width="38" height="51" viewBox="0 0 38 51">
                      <path d="M12.54 18.81c0-3.45 2.82-6.27 6.27-6.27 3.48 0 6.3 2.82 6.3 6.27 0 3.48-2.82 6.3-6.3 6.3-3.45 0-6.27-2.82-6.27-6.3zm-6.27 0c0 6.96 5.61 12.57 12.54 12.57 6.96 0 12.57-5.61 12.57-12.57 0-6.93-5.61-12.54-12.57-12.54-6.93 0-12.54 5.61-12.54 12.54zm-6.27 0c0-10.38 8.43-18.81 18.81-18.81 10.41 0 18.84 8.43 18.84 18.81 0 10.41-18.84 31.38-18.84 31.38s-18.81-20.97-18.81-31.38z"></path>
                    </svg>
                    Detroit, MI
                  </div>
                  <div class="type">
                    <svg xmlns="http://www.w3.org/2000/svg" width="43" height="43" viewBox="0 0 43 43">
                      <path d="M30.36 18.48c-1.11 0-1.98.87-1.98 1.98v13.86c0 1.08.87 1.98 1.98 1.98h3.96c1.08 0 1.98-.9 1.98-1.98v-13.86c0-1.11-.9-1.98-1.98-1.98zm-11.22-7.92c-1.11 0-1.98.87-1.98 1.98v21.78c0 1.08.87 1.98 1.98 1.98h3.96c1.08 0 1.98-.9 1.98-1.98v-21.78c0-1.11-.9-1.98-1.98-1.98zm-11.22 17.82c-1.08 0-1.98.87-1.98 1.98v3.96c0 1.08.9 1.98 1.98 1.98h3.96c1.08 0 1.98-.9 1.98-1.98v-3.96c0-1.11-.9-1.98-1.98-1.98zm-3.96-24.42h34.32v34.32h-34.32zm36.3 38.28c1.08 0 1.98-.9 1.98-1.98v-38.28c0-1.08-.9-1.98-1.98-1.98h-38.28c-1.08 0-1.98.9-1.98 1.98v38.28c0 1.08.9 1.98 1.98 1.98z"></path>
                    </svg>
                    Mid-level, Short Term
                  </div>
                </div>
                <a href="/" class="btn primary-btn icon"><span>APPLY FOR THIS</span></a>
              </div>
            </div>
          </div>
        </div>

      </section>
      
    </div>
  </div>
</main>

<?php get_footer(); ?>