<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header();
?>

<?php //GET BACKGROUND IMAGE
if ( has_post_thumbnail() ) {
  $thumb_id = get_post_thumbnail_id();
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
  $thumb_url = $thumb_url_array[0];
} else if ( is_archive() ) {
  $thumb_url_array = get_field('featured_events_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} else {
  $thumb_url_array = get_field('default_event_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} ?>




<main class="page-contents-container <?php if ( is_archive() ) { ?>grey-bg<?php } ?> ?> full-width">

  <!-- PAGE TITLE -->
  <section class="page-title-container full-width vertical-align-parent dark-bg" style="background-image: url('<?php echo $thumb_url ?>');">
    <div class="page-title-contents vertical-align-child">
      <?php if ( is_archive() ) { ?>
        <div class="page-icon" style="background-image: url('/wp-content/uploads/2016/10/icon-events.svg');"></div>
        <h1 class="page-title">Attend something greater.</h1>
        <p class="page-description">GOODWILL SPONSORS AND SUPPORTS THE COMMUNITY THROUGH A VARIETY OF EVENTS</p>
      <?php } else { ?>
        <?php 
          $start_date = tribe_get_start_date( null, false ); 
        $event_date = str_replace( ',', '' , $start_date);
        $event_date_parts = explode(" ", $event_date );
        $event_month = $event_date_parts[0];
        $event_year = $event_date_parts[2];
        $event_day = substr($event_date_parts[1], 0, -2);
        if ( is_null($event_year) ) {
          $event_year = date("Y");
        }
        ?>
        <h3><?php echo $event_month; ?> <?php echo $event_day; ?>, <?php echo $event_year; ?></h3>
        <h1 class="page-title"><?php the_title(); ?></h1>
      <?php } ?>
    </div>
    <div class="page-title-overlay"></div>
  </section>
  <?php if ( function_exists('yoast_breadcrumb') ) { ?>
    <div class="breadcrumbs-nav">
      <div class="max-width">
        <?php yoast_breadcrumb('<p>','</p>'); ?>
        <?php if( is_single() ) { ?><p class="post-date">Published: <?php the_date('m/d/y'); ?></p><?php } ?>
      </div>
    </div>
  <?php } ?>

  <!--SKIP NAV -->
  <a id="main-anchor" class="page-anchor"></a>

  <div class="sidebar-template max-width flex-container">

    <div class="page-main-contents">
        	<?php tribe_events_before_html(); ?>
        	<?php tribe_get_view(); ?>
        	<?php tribe_events_after_html(); ?>
    </div>

    <aside>
      <?php get_sidebar('events'); ?>
    </aside>
  </div>


<?php get_footer(); ?>