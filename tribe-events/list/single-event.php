<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$venue_details = tribe_get_venue_details();
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';
$organizer = tribe_get_organizer();
?>

  <div class="event-preview flex-container">
    <div class="event-date dark-bg">
    	<?php 
    		$start_date = tribe_get_start_date( null, false ); 
    		$event_date = str_replace( ',', '' , $start_date);
    		$event_date_parts = explode(" ", $event_date );
    		$event_month = $event_date_parts[0];
    		$event_year = $event_date_parts[2];
    		$event_day = substr($event_date_parts[1], 0, -2);
    		if ( $event_day < 10 ) {
    			$event_day = '0'.$event_day;
    		}
    		if ( is_null($event_year) ) {
    			$event_year = date("Y");
    		}
    	?>
      <p class="date">
        <span><?php echo $event_month; ?></span>
        <span><?php echo $event_day; ?></span>
        <span><?php echo $event_year; ?></span>
      </p>
      <?php if ( get_field('event_still_open') == 'Yes') { ?>
      	<a href="<?php echo esc_url( tribe_get_event_link() ); ?>/#event-reserve" class="btn primary-btn icon"><span>Reserve Now</span></a>
      <?php } ?>
    </div>
    <div class="event-contents">
      <div class="description">
        <a href="<?php echo esc_url( tribe_get_event_link() ); ?>">
          <h3><?php the_title() ?></h3>
          <?php if ( get_field('event_summary') ) { ?>
        	 <p><?php the_field('event_summary') ?></p>
           <div class="learn-more">View More
            <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
              <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
            </svg>
          </div>
          <?php } ?>
        </a>
      </div>
      <a href="http://maps.google.com/?q=<?php echo $full_address ?>" target="_blank" class="location"> 
	      <svg xmlns="http://www.w3.org/2000/svg" width="38" height="51" viewBox="0 0 38 51">
	        <path d="M12.54 18.81c0-3.45 2.82-6.27 6.27-6.27 3.48 0 6.3 2.82 6.3 6.27 0 3.48-2.82 6.3-6.3 6.3-3.45 0-6.27-2.82-6.27-6.3zm-6.27 0c0 6.96 5.61 12.57 12.54 12.57 6.96 0 12.57-5.61 12.57-12.57 0-6.93-5.61-12.54-12.57-12.54-6.93 0-12.54 5.61-12.54 12.54zm-6.27 0c0-10.38 8.43-18.81 18.81-18.81 10.41 0 18.84 8.43 18.84 18.81 0 10.41-18.84 31.38-18.84 31.38s-18.81-20.97-18.81-31.38z"></path>
	      </svg>
	      <?php 
		    	$venue_id = get_the_ID();
					$full_region = tribe_get_full_region( $venue_id );
					$full_address = tribe_get_address( $venue_id ) .' '. tribe_get_city( $venue_id ) .' '. tribe_get_region( $venue_id ) .' '. tribe_get_zip( $venue_id ) .' '. tribe_get_country( $venue_id );
					echo $full_address;
				?>
      </a>
      <div class="social-likes"></div>
    </div>
  </div>