<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

	<!-- SECONDARY NAVIGATION -->
  <nav class="secondary-nav max-width">
    <ul>
      <li class="menu-item"><a href="#event-details" class="smoothScroll">DETAILS</a></li>
      <?php if( have_rows('sponsors') ) { ?><li class="menu-item"><a href="#event-sponsors" class="smoothScroll">SPONSORS</a></li><?php } ?>
      <?php if( have_rows('speakers') ) { ?><li class="menu-item"><a href="#event-lineup" class="smoothScroll">LINEUP</a></li><?php } ?>
      <?php if( get_field('venue_description') ) { ?><li class="menu-item"><a href="#event-venue" class="smoothScroll">VENUE</a></li><?php } ?>
      <li class="menu-item"><a href="#event-directions" class="smoothScroll">DIRECTIONS</a></li>
      <?php if( get_field('sign_up') ) { ?><li class="menu-item"><a href="#event-reserve" class="smoothScroll">RESERVE NOW</a></li><?php } ?>
    </ul>
  </nav>

  <!-- DESCRIPTION -->
	<div class="max-width">
		<a id="event-details" class="page-anchor"></a>
		<h2>Details</h2>
		<?php the_content(); ?>
	</div>

  <!-- SPONSORS -->
  <?php if( have_rows('sponsors') ) { ?>
    <div class="event-sponsors full-width">
      <a id="event-sponsors" class="page-anchor"></a>
      <h2>Sponsors</h2>
      <div class="sponsor-container flex-container full-width">
        <?php while ( have_rows('sponsors') ) : the_row(); ?>

          <div class="one-third">
            <div class="vertical-align-parent">
              <div class="vertical-align-child">
                <?php $image = get_sub_field('logo'); ?>
                <a href="<?php the_sub_field('url'); ?>" target="_blank"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
              </div>
            </div>
          </div>

        <?php endwhile; ?>
      </div>
    </div>
  <?php } else {
    // no rows found
  } ?>

  <!-- SPEAKERS -->
  <?php if( have_rows('speakers') ) { ?>
    <div class="event-speakers">
      <a id="event-lineup" class="page-anchor"></a>
      <h2>Lineup</h2>
      <div class="speaker-slider-container full-width">
        <?php while ( have_rows('speakers') ) : the_row(); ?>

          <div class="slide">
            <div class="flip-container card-flip">
              <div class="flipper">
                <div class="front">
                  <?php $image = get_sub_field('headshot'); ?>
                  <div class="card-img" style="background-image: url('<?php echo $image['url']; ?>');"></div>
                  <h3 class="name"><?php the_sub_field('name'); ?></h3>
                  <p class="title"><?php the_sub_field('job_title'); ?></p>
                  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="34" viewBox="0 0 26 34">
                    <path d="M0 33.6v-33.6l25.98 17.88z" fill="#a5cb5a"></path>
                  </svg>
                </div>
                <div class="back">
                  <h3 class="name"><?php the_sub_field('name'); ?></h3>
                  <p class="title"><?php the_sub_field('job_title'); ?></p>
                  <p><?php the_sub_field('short_description'); ?></p>
                  <?php if ( get_sub_field('facebook_url') ) { ?>
                    <a href="<?php the_sub_field('facebook_url'); ?>" target="_blank" class="facebook social-link">
                      <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.113 430.114" enable-background="new 0 0 430.113 430.114">
                        <path d="M158.081 83.3v59.218h-43.385v72.412h43.385v215.183h89.122v-215.177h59.805s5.601-34.721 8.316-72.685h-67.784v-49.511c0-7.4 9.717-17.354 19.321-17.354h48.557v-75.385h-66.021c-93.519-.005-91.316 72.479-91.316 83.299z"></path>
                      </svg>
                      <span>Facebook Profile</span>
                    </a>
                  <?php } ?>
                  <?php if ( get_sub_field('twitter_url') ) { ?>
                    <a href="<?php the_sub_field('twitter_url'); ?>" target="_blank" class="twitter social-link">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612" width="512" height="512" enable-background="new 0 0 612 612">
                        <path d="M612 116.258c-22.525 9.981-46.694 16.75-72.088 19.772 25.929-15.527 45.777-40.155 55.184-69.411-24.322 14.379-51.169 24.82-79.775 30.48-22.907-24.437-55.49-39.658-91.63-39.658-69.334 0-125.551 56.217-125.551 125.513 0 9.828 1.109 19.427 3.251 28.606-104.326-5.24-196.835-55.223-258.75-131.174-10.823 18.51-16.98 40.078-16.98 63.101 0 43.559 22.181 81.993 55.835 104.479-20.575-.688-39.926-6.348-56.867-15.756v1.568c0 60.806 43.291 111.554 100.693 123.104-10.517 2.83-21.607 4.398-33.08 4.398-8.107 0-15.947-.803-23.634-2.333 15.985 49.907 62.336 86.199 117.253 87.194-42.947 33.654-97.099 53.655-155.916 53.655-10.134 0-20.116-.612-29.944-1.721 55.567 35.681 121.536 56.485 192.438 56.485 230.948 0 357.188-191.291 357.188-357.188l-.421-16.253c24.666-17.593 46.005-39.697 62.794-64.861z"></path>
                      </svg>
                      <span>Twitter Profile</span>
                    </a>
                  <?php } ?>
                  <?php if ( get_sub_field('linkedin_url') ) { ?>
                    <a href="<?php the_sub_field('linkedin_url'); ?>" target="_blank" class="linkedin social-link">
                      <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 430.117 430.117" enable-background="new 0 0 430.117 430.117">
                        <path d="M430.117 261.543v159.017h-92.188v-148.367c0-37.271-13.334-62.707-46.703-62.707-25.473 0-40.632 17.142-47.301 33.724-2.432 5.928-3.058 14.179-3.058 22.477v154.873h-92.219s1.242-251.285 0-277.32h92.21v39.309l-.606.896h.606v-.896c12.251-18.869 34.13-45.824 83.102-45.824 60.673-.001 106.157 39.636 106.157 124.818zm-377.934-251.985c-31.548 0-52.183 20.693-52.183 47.905 0 26.619 20.038 47.94 50.959 47.94h.616c32.159 0 52.159-21.317 52.159-47.94-.606-27.212-20-47.905-51.551-47.905zm-46.706 411.002h92.184v-277.32h-92.184v277.32z"></path>
                      </svg>
                      <span>LinkedIn Profile</span>
                    </a>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>

        <?php endwhile; ?>
      </div>
    </div>
  <?php } else { 
    // NO SLIDES
  } ?>

  <!-- VENUE -->
  <?php if( get_field('venue_featured_image') ) { ?>
    <div class="event-venue">
      <a id="event-venue" class="page-anchor"></a>
      <h2>Venue</h2>
      <?php $image = get_field('venue_featured_image'); ?>
      <div class="venue-featured" style="background-image: url(<?php echo $image['url']; ?>);"></div>
      <?php the_field('venue_description'); ?>
      <div class="venue-gallery flex-container">
        <?php 
          $images = get_field('venue_gallery');
          $i = 0; 
        ?>
        <?php foreach( $images as $image ): $i++;?>

          <div class="gallery-item one-third" style="background-image: url(<?php echo $image['sizes']['medium']; ?>);"></div>

         <?php endforeach; ?>
      </div>
    </div>
  <?php } ?>

  <!-- DIRECTIONS -->
  <div class="event-directions">
    <a id="event-directions" class="page-anchor"></a>
    <h2>Directions</h2>
    <?php tribe_get_template_part( 'modules/meta' ); ?>
    <?php 
    	$venue_id = get_the_ID();
			$full_region = tribe_get_full_region( $venue_id );
			$full_address = tribe_get_address( $venue_id ) .' '. tribe_get_city( $venue_id ) .' '. tribe_get_region( $venue_id ) .' '. tribe_get_zip( $venue_id ) .' '. tribe_get_country( $venue_id );
		?>
    <a href="http://maps.google.com/?q=<?php echo $full_address ?>" target="_blankk" class="btn primary"><span>Directions</span></a>
  </div>


  <!-- RESERVE -->
  <?php if( get_field('sign_up') && get_field('event_still_open') == 'Yes' ) { ?>
    <div class="event-reserve">
       <a id="event-reserve" class="page-anchor"></a>
      <h2>Reserve Now</h2>
      <?php the_field('sign_up'); ?>
    </div>
  <?php } ?>

    <!-- BACK TO TOP -->
  <a href="#main-anchor" class="back-to-top skip smoothScroll hidden">
    <svg xmlns="http://www.w3.org/2000/svg" width="53" height="51" viewBox="0 0 53 51">
      <path d="M22 31.28v-10.28l7.77 5.47z" fill="#005c75"></path>
    </svg>
    <span>Back to Top</span>
  </a>
